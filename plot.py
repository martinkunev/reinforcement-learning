#!/usr/bin/env python3

import matplotlib.pyplot as pyplot
import math
import sys
import re

def scores(filename):
	last = {}

	f = open(filename, "r")
	for line in f.readlines():
		"""match = re.match(r"^\+([01][.][0-9]+), ([01][.][0-9]+), -([01][.][0-9]+) \| [0-9][.][0-9] total=-?[01][.][0-9]+", line)
		if match != None:
			win, draw, loss = float(match.group(1)), float(match.group(2)), float(match.group(3))
			games = win + draw + loss
			yield (loss / games, draw / games)"""

		#match = re.match(r"^total=(-?[0-9]+[.][0-9]+) \| \+([01][.][0-9]+), ([01][.][0-9]+), -([01][.][0-9]+) \| [0-9]+[.][0-9]", line)

		match = re.match(r"^> ([0-9]) ([01][.][0-9]+) ([01][.][0-9]+) ([01][.][0-9]+)", line)
		#match = re.match(r"^> ([0-9]) ([1-9][0-9]*)", line)
		if match != None:
			key = int(match.group(1))
			if (key == 0):
				last = {}
				if len(last) == 3:
					yield last
			last[key] = [float(match.group(2)), float(match.group(3)), float(match.group(4))]
			#last[key] = [float(match.group(2))]
		elif (last != None) and (re.match(r"# [0-9]+", line) != None):
			yield last
		"""elif (last != None) and (re.match(r"# [0-9]+", line) != None):
			if len(last) == 3:
				yield last"""

	f.close()

lines = [[], [], []]

for result in scores(sys.argv[1]):
	for opponent in result:
		lines[opponent].append(result[opponent])

x = list(range(len(lines[0])))
assert len(lines[0]) == len(lines[1]) == len(lines[2])

def plot(x, data, axis, title):
	axis.set_title(title)

	y_win = [item[0] for item in data]
	y_draw = [item[1] for item in data]
	y_loss = [item[2] for item in data]

	axis.plot(x, y_win, color="green")
	axis.plot(x, y_draw, color="gray")
	axis.plot(x, y_loss, color="red")

	mean = sum([d[0] for d in data]) / len(data)
	axis.axline((x[0], mean), (x[-1], mean), color="black")

fig, axes = pyplot.subplots(nrows=3)

plot(x, lines[0], axes[0], "random")
plot(x, lines[1], axes[1], "weak heuristic")
plot(x, lines[2], axes[2], "strong heuristic")

pyplot.show()
