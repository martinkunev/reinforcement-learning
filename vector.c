#include <stdlib.h>

#include "vector.h"

struct vector *vector_alloc(size_t rows, size_t columns)
{
	struct vector *result = malloc(sizeof(struct vector) + rows * columns * sizeof(*result->buffer));
	if (!result)
		return 0;
	result->shape[0] = rows;
	result->shape[1] = columns;
	return result;
}

double *vector_get(struct vector *restrict vector, size_t row, size_t column)
{
	return &vector->buffer[row * vector->shape[1] + column];
}
