enum {BOARD_SIZE = 8, INPUT_SIZE = 2 * BOARD_SIZE * BOARD_SIZE, OUTPUT_SIZE = BOARD_SIZE * BOARD_SIZE};

enum {HISTORY_LIMIT = 65536, TRAIN_BATCH = 256, TRAIN_THREADS = 8, TRAIN_EPISODES = 8, PLAY_EPISODES = 64};

enum {AGENTS_COUNT = 8, AGENTS_HISTORY = 8};

static const double DISCOUNT_FACTOR = 0.99;
//static const double DISCOUNT_FACTOR = 1;

enum {INVALID = 127};

struct position
{
	int y, x;
};

struct line
{
	struct position fields[BOARD_SIZE];
	size_t count;
};

struct score
{
	struct position action;
	double value;
};

struct actions
{
	size_t count;
	double score;
	struct position moves[];
};

struct reversi
{
	struct state
	{
		unsigned char board[BOARD_SIZE][BOARD_SIZE];
		unsigned char player;
		unsigned time;
	} state;

	struct position moves[BOARD_SIZE * BOARD_SIZE];
	unsigned char moves_board[BOARD_SIZE][BOARD_SIZE];
	size_t moves_count;

	// reversi-specific
	int stones_difference; // difference between the number of stones of the two players
};

struct player
{
	struct score (*play)(const struct reversi *restrict, const struct player *const restrict *restrict, double);
	void (*utility)(const struct reversi *restrict, double *restrict, const void *); // > 0 for max and < 0 for min
	struct nn *network;
	const char *id; // used to identify the player in logs
	unsigned lookahead;
};

int game_score(const struct state *restrict state);
double ai_score(struct nn *restrict nn, const struct state *restrict state);
void input_set(double *restrict input, const struct state *restrict state, unsigned augment);

enum {TRANSFORM_SWITCH_XY = 0x1, TRANSFORM_INVERSE_X = 0x10, TRANSFORM_INVERSE_Y = 0x100};

struct position position_transform(struct position base, unsigned symmetry);

struct reversi *transition_alloc(const struct reversi *restrict game, struct position action);

void neighbors(struct reversi *restrict game);
struct reversi *move_alloc(const struct reversi *restrict game, struct position action);

void heuristic_utility0(const struct reversi *restrict game, double *restrict scores, const void *argument);
void heuristic_utility1(const struct reversi *restrict game, double *restrict scores, const void *argument);
void ai_utility(const struct reversi *restrict game, double *restrict scores, const void *argument);
void zero_utility(const struct reversi *restrict game, double *restrict scores, const void *argument);

struct score bot_play(const struct reversi *restrict game, const struct player *const restrict *restrict players, double explore_rate);

void print(const struct reversi *restrict game);

static inline unsigned char sign2player(char sign)
{
	switch (sign)
	{
	case 'x':
		return 0;

	case 'o':
		return 1;

	case ' ':
		return 2;

	default:
		return INVALID;
	}
}

static inline unsigned char player2sign(unsigned char player)
{
	return (unsigned char)"xo"[player];
}

static inline int action_index(struct position action)
{
	return action.y * BOARD_SIZE + action.x;
}

struct reversi *game_alloc(void);
struct reversi *move_alloc(const struct reversi *restrict game, struct position action);
void neighbors(struct reversi *restrict game);
int game_score(const struct state *restrict state);
