#include <stdio.h>

#include <Python.h>
#include "structmember.h"

#include "nn.h"
#include "game.h"
#include "io.h"

// https://realpython.com/build-python-c-extension-module/

struct python_player {
	PyObject_HEAD
	struct nn *network;
	int lookahead;
};

static PyObject *new(PyTypeObject *type, PyObject *args, PyObject *kwds);
static void dealloc(struct python_player *self);
static int init(struct python_player *self, PyObject *args, PyObject *kwds);
static PyObject *play(struct python_player *self, PyObject *args);

static PyMethodDef object_methods[] =
{
	{"play", (PyCFunction)play, METH_VARARGS, "Return the next move of the bot."},
	{NULL}
};

static PyMemberDef object_members[] = {
	{NULL}
};

static PyTypeObject bot_type = {
	PyObject_HEAD_INIT(NULL)
	.tp_name = "reversibot.nn",
	.tp_basicsize = sizeof(struct python_player),
	.tp_dealloc = (destructor)dealloc,
	.tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	.tp_doc = "Reversi bot",
	.tp_methods = object_methods,
	.tp_members = object_members,
	.tp_init = (initproc)init,
	.tp_new = new,
};

static PyMethodDef module_methods[] =
{
	{NULL}  /* Sentinel */
};

static struct PyModuleDef module =
{
	PyModuleDef_HEAD_INIT,
	"reversibot", /* name of module */
	"Reversi bot using a neural network.\n", /* module documentation, may be NULL */
	-1,   /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
	module_methods
};

static PyObject *new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
	struct python_player *self = (struct python_player *)type->tp_alloc(type, 0);
	self->network = 0;
	return (PyObject *)self;
}

static void dealloc(struct python_player *self)
{
	free(self->network);
	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int init(struct python_player *self, PyObject *args, PyObject *kwds)
{
	char *filename;

	if (!PyArg_ParseTuple(args, "si", &filename, &self->lookahead))
		return -1;

	self->network = nn_load_file(filename);
	if (!self->network)
		return -1;

	return 0;
}

static PyObject *play(struct python_player *self, PyObject *args)
{
	char *board;
	struct reversi *game;
	int time = -4;
	size_t i;
	struct score score;

	struct player players[2];
	const struct player *const agents[] = {&players[0], &players[1]};

	PyObject *action;

	if (!PyArg_ParseTuple(args, "s", &board))
		return 0;

	if (strlen(board) != sizeof(game->state.board))
		return 0;

	game = game_alloc();
	if (!game)
		return 0;

	for(i = 0; i < sizeof(game->state.board); i += 1)
	{
		switch (board[i])
		{
		case 'x':
		case 'o':
			time += 1;
			break;

		case ' ':
			break;

		default:
			return 0;
		}
	}

	memcpy(game->state.board, board, sizeof(game->state.board));
	game->state.player = time % 2;
	game->state.time = time;
	neighbors(game);

	players[game->state.player] = (struct player){.play = &bot_play, .utility = &ai_utility, .network = self->network, .lookahead = self->lookahead, .id = ""};
	players[!game->state.player] = (struct player){.play = 0, .utility = &ai_utility, .network = self->network, .lookahead = self->lookahead, .id = ""};

	score = (*players[game->state.player].play)(game, agents, time ? 0 : 1);
	action = PyLong_FromLong(score.action.y * BOARD_SIZE + score.action.x);

	printf("%i %i\n", score.action.y, score.action.x);

	free(game);

	return action;
}

PyMODINIT_FUNC PyInit_reversibot(void)
{
	PyObject *m;

	if (PyType_Ready(&bot_type) < 0)
		return 0;

	m = PyModule_Create(&module);
	if (!m)
		return 0;

	Py_INCREF(&bot_type);
	if (PyModule_AddObject(m, "nn", (PyObject *)&bot_type) < 0)
	{
		Py_DECREF(&bot_type);
		Py_DECREF(m);
		return 0;
	}

	return m;
}
