static inline double min(double a, double b)
{
	return (a < b) ? a : b;
}
static inline double max(double a, double b)
{
	return (a > b) ? a : b;
}

static inline int rand_bernoulli(double probability)
{
	// when probability == 0, always returns 0
	// when probability == 1, always returns 1
	return (rand() / (RAND_MAX + 1.0)) < probability;
}

static inline int sign(double n)
{
	return (n > 0) - (n < 0);
}

int argbest_random(double (*best)(double, double), const double *restrict scores, size_t scores_count);
int argsoftmax_random(double (*best)(double, double), const double *restrict scores, size_t scores_count);

void *memdup(const void *data, size_t size);
