struct experience
{
	struct
	{
		struct state state, state_next;
		struct position action;
		int game_over;
		double reward;
	} samples[HISTORY_LIMIT];
	size_t count, last;
};

struct train
{
	struct player behavior, opponent;
	double explore_rate, learn_rate;
	struct experience *experience;
};

void *main_train(void *argument);
void *main_play(void *argument);

void networks_merge(struct nn *restrict target, struct gradient *restrict gradients, size_t count, double learn_rate);
