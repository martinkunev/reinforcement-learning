#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <stdio.h>

#define RELU_LEAK 0.0001

#include "vector.h"
#include "utils.h"
#include "nn.h"

static inline double activation_identity(double value)
{
	return value;
}
static inline double derivative_identity(double *restrict activations, unsigned neuron)
{
	return 1;
}

static inline double activation_logistic(double value)
{
	return 2 / (1 + exp(-value)) - 1; // TODO scaled logistic in [-1, 1]
}
static inline double derivative_logistic(double *restrict activations, unsigned neuron)
{
	return 2 * activations[neuron] * (1 - activations[neuron]);
}

static inline double activation_softplus(double value)
{
	if (isinf(log(exp(value) + 1)))
		abort();
	return log(exp(value) + 1);
}
static inline double derivative_softplus(double *restrict activations, unsigned neuron)
{
	double e_pow_a = exp(activations[neuron]);
	if (isinf(e_pow_a - 1) / e_pow_a)
		abort();
	return (e_pow_a - 1) / e_pow_a;
}

static inline double activation_leakyrelu(double value)
{
	//return (value > 0) ? value : 0;
	return (value >= 0) ? value : (value * RELU_LEAK);
}
static inline double derivative_leakyrelu(double *restrict activations, unsigned neuron)
{
	//return activations[neuron] != 0;
	return activations[neuron] >= 0 ? 1 : RELU_LEAK;
}

static inline double activation_softmax_denormalized(double value)
{
	// softmax is strictly monotonically increasing.
	// Don't do any calculations to improve performance.
	return value;
}
static inline double derivative_softmax_denormalized(double *restrict activations, unsigned neuron) // TODO rename
{
	// TODO fix this
	return exp(activations[neuron]);
}

void normalize(double *restrict activations, size_t units_count)
{
	// TODO do exp of each activation first
	double total = 0;
	size_t p;
	for(p = 0; p < units_count; p += 1)
		total += activations[p];
	for(p = 0; p < units_count; p += 1)
		activations[p] /= total;
}

static void layer_set(struct nn *restrict network, const struct layer *restrict layers, size_t layer)
{
	if (layers[layer].type == LAYER_CONVOLUTION)
	{
		network->layers[layer].count = layers[layer].x * layers[layer].y * layers[layer].z;
		network->layers[layer].kernel_size = layers[layer].depth * layers[layer].width * layers[layer].height;
		network->layers[layer].kernels_count = layers[layer].x;

		// WARNING: Assume the convolution reduces the depth dimension to 1.
		network->layers[layer].columns = layers[layer].y;
		network->layers[layer].rows = layers[layer].z;

		network->layers[layer].depth = layers[layer].depth;
		network->layers[layer].width = layers[layer].width;
		network->layers[layer].height = layers[layer].height;
	}
	else
	{
		network->layers[layer].kernels_count = network->layers[layer].count = layers[layer].x;
		if (layer > 0)
			network->layers[layer].kernel_size = network->layers[layer - 1].count;

		network->layers[layer].rows = network->layers[layer].columns = 1;
	}

	if (layer >= 2)
	{
		network->layers[layer].offset_neurons = network->layers[layer - 1].offset_neurons + network->layers[layer - 1].count;

		if (network->layers[layer - 1].kernel_size != network->layers[layer - 2].count) // convolution
			network->layers[layer].offset_weights = network->layers[layer - 1].offset_weights + network->layers[layer - 1].kernels_count * network->layers[layer - 1].kernel_size;
		else
			network->layers[layer].offset_weights = network->layers[layer - 1].offset_weights + network->layers[layer - 2].count * network->layers[layer - 1].count;
		network->layers[layer].offset_biases = network->layers[layer - 1].offset_biases + network->layers[layer - 1].count;
	}
	else
	{
		network->layers[layer].offset_neurons = layer ? layers[0].x : 0;
		network->layers[layer].offset_weights = 0;
		network->layers[layer].offset_biases = 0;
	}

	switch (layers[layer].activation)
	{
	case ACTIVATION_IDENTITY:
		network->layers[layer].activation = &activation_identity;
		network->layers[layer].derivative = &derivative_identity;
		break;

	case ACTIVATION_LOGISTIC:
		network->layers[layer].activation = &activation_logistic;
		network->layers[layer].derivative = &derivative_logistic;
		break;

	case ACTIVATION_SOFTPLUS:
		network->layers[layer].activation = &activation_softplus;
		network->layers[layer].derivative = &derivative_softplus;
		break;

	case ACTIVATION_RELU:
		network->layers[layer].activation = &activation_leakyrelu;
		network->layers[layer].derivative = &derivative_leakyrelu;
		break;

	case ACTIVATION_SOFTMAX:
		network->layers[layer].activation = &activation_softmax_denormalized;
		network->layers[layer].derivative = &derivative_softmax_denormalized;
		break;
	}
}

struct nn *nn_init(const struct layer *restrict layers, size_t layers_count)
{
	size_t size_layers;
	size_t weights_total, biases_total;
	size_t layer_prev_count;
	size_t layer, i;
	double deviation;

	struct nn *network;

	// Calculate the number of weights and biases.
	weights_total = 0;
	biases_total = 0;
	layer_prev_count = layers[0].x;
	for(layer = 1; layer < layers_count; layer += 1)
	{
		size_t layer_count = layers[layer].x;
		if (layers[layer].type == LAYER_CONVOLUTION)
		{
			unsigned kernel_weights = layers[layer].depth * layers[layer].width * layers[layer].height;
			weights_total += layers[layer].x * kernel_weights;
			layer_count *= layers[layer].y * layers[layer].z;
		}
		else
			weights_total += layer_count * layer_prev_count;
		biases_total += layer_count;
		layer_prev_count = layer_count;
	}

	// Allocate memory.
	size_layers = layers_count * sizeof(*network->layers);
	network = malloc(sizeof(*network) + size_layers + weights_total * sizeof(*network->weights) + biases_total * sizeof(*network->biases));
	if (!network)
		return 0;
	network->weights = (double *)((char *)(network + 1) + size_layers);
	network->biases = (double *)((char *)(network + 1) + size_layers) + weights_total;

	network->layers_count = layers_count;
	for(layer = 0; layer < layers_count; layer += 1)
		layer_set(network, layers, layer);

	network->weights_total = weights_total;
	network->biases_total = biases_total;
	network->neurons_total = network->layers[0].count + network->biases_total;

	// TODO this is for ReLU. make sure it is okay
	// TODO how do I initialize weights in a convolutional layer
	if (network->layers[1].activation == &activation_leakyrelu)
		deviation = sqrt(6.0 / network->layers[0].count); // relu
	else
		deviation = sqrt(3.0 / network->layers[0].count); // logistic
	for(i = 0; i < network->weights_total; i += 1)
		network->weights[i] = (rand() / (RAND_MAX / 2.0) - 1.0) * deviation;
	for(i = 0; i < network->biases_total; i += 1)
		network->biases[i] = 0;

	return network;
}

struct nn *nn_load(void *data)
{
	size_t size_layers;
	size_t weights_total, biases_total;
	size_t layer;
	size_t layer_prev_count;

	const size_t *s = data;
	const struct layer *layers = (const struct layer *)(s + 2);
	const double *d = (const double *)(layers + s[0]);
	const double *weights, *biases;
	size_t layers_count = s[0];

	struct nn *network;

	// Calculate the number of weights and biases.
	weights_total = 0;
	biases_total = 0;
	layer_prev_count = layers[0].x;
	for(layer = 1; layer < layers_count; layer += 1)
	{
		size_t layer_count = layers[layer].x;
		if (layers[layer].type == LAYER_CONVOLUTION)
		{
			unsigned kernel_weights = layers[layer].depth * layers[layer].width * layers[layer].height;
			weights_total += layers[layer].x * kernel_weights;
			layer_count *= layers[layer].y * layers[layer].z;
		}
		else
			weights_total += layer_count * layer_prev_count;
		biases_total += layer_count;
		layer_prev_count = layer_count;
	}

	// Allocate memory.
	size_layers = layers_count * sizeof(*network->layers);
	network = malloc(sizeof(*network) + size_layers + weights_total * sizeof(*network->weights) + biases_total * sizeof(*network->biases));
	if (!network)
		return 0;
	network->weights = (double *)((char *)(network + 1) + size_layers);
	network->biases = (double *)((char *)(network + 1) + size_layers) + weights_total;

	network->layers_count = layers_count;
	for(layer = 0; layer < network->layers_count; layer += 1)
		layer_set(network, layers, layer);

	network->weights_total = weights_total;
	network->biases_total = biases_total;
	network->neurons_total = s[1];

	weights = d;
	memcpy(network->weights, weights, network->weights_total * sizeof(*weights));

	biases = d + network->weights_total;
	memcpy(network->biases, biases, network->biases_total * sizeof(*biases));

	return network;
}

struct nn *nn_clone(struct nn *restrict source)
{
	size_t size_layers = source->layers_count * sizeof(*source->layers);
	size_t size = sizeof(*source) + size_layers + source->weights_total * sizeof(*source->weights) + source->biases_total * sizeof(*source->biases);
	struct nn *clone = memdup(source, size);
	if (!clone)
		return 0;
	clone->weights = (double *)((char *)(clone + 1) + size_layers);
	clone->biases = (double *)((char *)(clone + 1) + size_layers) + source->weights_total;
	return clone;
}

void nn_dump(const char *restrict filename, struct nn *restrict network, const char *restrict comment)
{
	size_t i;
	int f;

	f = creat(filename, 0644);
	write(f, comment, strlen(comment));
	write(f, &network->layers_count, sizeof(network->layers_count));
	write(f, &network->neurons_total, sizeof(network->neurons_total));
	for(i = 0; i < network->layers_count; i += 1)
	{
		struct layer layer;

		if (!i)
			layer.type = LAYER_INPUT;
		else if (network->layers[i].kernel_size != network->layers[i - 1].count)
			layer.type = LAYER_CONVOLUTION;
		else
			layer.type = LAYER_DENSE;

		layer.x = network->layers[i].count;
		if (network->layers[i].activation == &activation_identity)
			layer.activation = ACTIVATION_IDENTITY;
		else if (network->layers[i].activation == &activation_logistic)
			layer.activation = ACTIVATION_LOGISTIC;
		else if (network->layers[i].activation == &activation_softplus)
			layer.activation = ACTIVATION_SOFTPLUS;
		else if (network->layers[i].activation ==  &activation_leakyrelu)
			layer.activation = ACTIVATION_RELU;

		layer.x = network->layers[i].kernels_count;
		layer.y = network->layers[i].columns;
		layer.z = network->layers[i].rows;

		layer.depth = network->layers[i].depth;
		layer.width = network->layers[i].width;
		layer.height = network->layers[i].height;

		write(f, &layer, sizeof(layer));
	}

	write(f, network->weights, network->weights_total * sizeof(*network->weights));
	write(f, network->biases, network->biases_total * sizeof(*network->biases));

	close(f);
}

static inline size_t index_neuron(const struct nn *restrict network, unsigned layer, unsigned unit)
{
	assert(unit < network->layers[layer].count);
	return network->layers[layer].offset_neurons + unit;
}

static inline size_t index_neuron_convolution(const struct nn *restrict network, unsigned layer, unsigned kernel, unsigned unit)
{
	assert(unit < network->layers[layer].count);
	return network->layers[layer].offset_neurons + kernel * (network->layers[layer].columns * network->layers[layer].rows) + unit;
}

static inline size_t index_bias(const struct nn *restrict network, unsigned layer, unsigned unit)
{
	assert(layer);
	assert(unit < network->layers[layer].count);
	return network->layers[layer].offset_biases + unit;
}

static inline size_t index_weight(const struct nn *restrict network, unsigned layer, unsigned unit, unsigned argument)
{
	assert(layer);
	assert(unit < network->layers[layer].count);
	assert(argument < network->layers[layer].kernels_count * network->layers[layer].kernel_size);
	return network->layers[layer].offset_weights + unit * network->layers[layer - 1].count + argument;
}

/* TODO sample format should support regression
static double error_L2(const struct nn *restrict network, const struct sample *restrict sample, double *restrict activations)
{
	double error = 0;

	// TODO don't hardcode number of outputs
	size_t out;
	for(out = 0; out < 2; out += 1)
		error += (activations[out] - sample->output[out]) * (activations[out] - sample->output[out]);
	return error;
}
*/

/*static inline double loss_crossentropy(const struct nn *restrict network, struct vector *restrict y, double *restrict activations)
{
	size_t out = index_neuron(network, network->layers_count - 1, y);
	return - log(activations[out]);
}

static inline double derivative_softmax_crossentropy(const struct nn *restrict network, double label, double *restrict activations)
{
	size_t out = index_neuron(network, network->layers_count - 1, label);
	return activations[out] - 1;
}*/

static inline const double *nn_output(const struct nn *restrict network, const double *restrict activations)
{
	// TODO ideally this should return a slice
	return activations + network->layers[network->layers_count - 1].offset_neurons;
}

const double *nn_regression(const struct nn *restrict network, double *restrict activations)
{
	size_t layer, kernel;

	for(layer = 1; layer < network->layers_count; layer += 1)
	{
		for(kernel = 0; kernel < network->layers[layer].kernels_count; kernel += 1)
		{
			double bias = network->biases[index_bias(network, layer, kernel)];
			size_t i;

			if (network->layers[layer].kernel_size != network->layers[layer - 1].count) // convolution
			{
				size_t offsets = network->layers[layer].columns * network->layers[layer].rows;
				size_t input_width = (network->layers[layer].columns + network->layers[layer].width - 1) * network->layers[layer].depth;
				size_t kernel_columns = network->layers[layer].depth * network->layers[layer].width;

				// Calculate activation for each output unit.
				for(i = 0; i < offsets; i += 1)
				{
					size_t unit = kernel * offsets + i;
					size_t offset_x = (i % network->layers[layer].columns) * network->layers[layer].depth;
					size_t offset_y = i / network->layers[layer].columns;
					size_t kernel_x, kernel_y;

					double value = bias;

					for(kernel_y = 0; kernel_y < network->layers[layer].height; kernel_y += 1)
					{
						for(kernel_x = 0; kernel_x < kernel_columns; kernel_x += 1)
						{
							double weight = network->weights[index_weight(network, layer, kernel, kernel_y * kernel_columns + kernel_x)];
							size_t n = (offset_y + kernel_y) * input_width + (offset_x + kernel_x);
							double activation = activations[index_neuron(network, layer - 1, n)];
							value += weight * activation;
						}
					}
					activations[index_neuron(network, layer, unit)] = (*network->layers[layer].activation)(value);
				}
			}
			else
			{
				double value = bias;
				for(i = 0; i < network->layers[layer].kernel_size; i += 1)
				{
					double activation = activations[index_neuron(network, layer - 1, i)];
					value += network->weights[index_weight(network, layer, kernel, i)] * activation;
				}
				activations[index_neuron(network, layer, kernel)] = (*network->layers[layer].activation)(value);
			}
		}
	}

	// WARNING: For softmax, the output is not normalized TODO it shouldn't be touched at all

	return nn_output(network, activations);
}

struct gradient nn_backpropagate(const struct nn *restrict network, size_t samples_count, struct vector *restrict y, double *restrict *restrict activations, double *restrict gradient, struct gradient *restrict g)
{
	size_t s, i, unit;
	size_t in, out;
	unsigned layer;
	double *weights, *biases;

	if (g)
	{
		weights = g->weights;
		biases = g->biases;
	}
	else
	{
		weights = malloc(network->weights_total * sizeof(*weights));
		biases = malloc(network->biases_total * sizeof(*biases));
		if (!weights || !biases)
		{
			free(weights);
			abort();
		}
		memset(weights, 0, network->weights_total * sizeof(*weights));
		memset(biases, 0, network->biases_total * sizeof(*biases));
	}

	for(s = 0; s < samples_count; s += 1)
	{
		// Calculate error gradients with respect to each non-input neuron's activation.

		for(i = 0; i < network->neurons_total; i += 1)
			gradient[i] = 0;

		layer = network->layers_count - 1;
		for(unit = 0; unit < network->layers[layer].count; unit += 1)
		{
			in = index_neuron(network, layer, unit);
			gradient[in] = (activations[s][in] - *vector_get(y, s, unit)) * (*network->layers[layer].derivative)(activations[s], in);
		}
		/*in = index_neuron(network, network->layers_count - 1, (size_t)*vector_get(y, s, 0));
		gradient[in] = derivative_softmax_crossentropy(network, *vector_get(y, s, 0), activations[s]);
//printf("del error %u: %f\n", in, gradient[in], derivative_crossentropy(network, *vector_get(y, s, 0), activations[s]));
		if (isinf(gradient[in]))
			abort();*/

		for(layer = network->layers_count - 2; layer; layer -= 1)
		{
			for(unit = 0; unit < network->layers[layer].count; unit += 1)
			{
				in = index_neuron(network, layer, unit);

				if (network->layers[layer + 1].kernel_size != network->layers[layer].count) // next layer is convolution
				{
					// Loop through the relevant activations for the input neuron unit.

					size_t input_width = (network->layers[layer + 1].columns + network->layers[layer + 1].width - 1) * network->layers[layer + 1].depth;
					size_t input_height = (network->layers[layer + 1].rows + network->layers[layer + 1].height - 1);
					size_t kernel_columns = network->layers[layer + 1].depth * network->layers[layer + 1].width;

					size_t offset_x = unit % input_width;
					size_t offset_y = unit / input_width;

					// Calculate the (inclusive) range of kernel weights used with the input unit.
					size_t x_from = max(0, (int)offset_x + (int)kernel_columns - (int)input_width);
					size_t x_to = min((int)kernel_columns - 1, (int)offset_x);
					size_t y_from = max(0, (int)offset_y + (int)network->layers[layer + 1].height - (int)input_height);
					size_t y_to = min((int)network->layers[layer + 1].height - 1, (int)offset_y);

					size_t kernel, kernel_x, kernel_y;

					for(kernel = 0; kernel < network->layers[layer + 1].kernels_count; kernel += 1)
					{
						for(kernel_y = y_from; kernel_y <= y_to; kernel_y += 1)
						{
							for(kernel_x = x_from; kernel_x <= x_to; kernel_x += 1)
							{
								size_t weight = index_weight(network, layer + 1, kernel, kernel_y * kernel_columns + kernel_x);
								size_t i = (offset_y - kernel_y) * network->layers[layer + 1].columns + (offset_x - kernel_x);
								size_t out = index_neuron_convolution(network, layer + 1, kernel, i);
								gradient[in] += gradient[out] * network->weights[weight] * (*network->layers[layer].derivative)(activations[s], in);
							}
						}
					}
				}
				else
				{
					for(i = 0; i < network->layers[layer + 1].count; i += 1)
					{
						double weight = network->weights[index_weight(network, layer + 1, i, unit)];
						out = index_neuron(network, layer + 1, i);
						gradient[in] += gradient[out] * weight * (*network->layers[layer].derivative)(activations[s], in);
					}
				}
			}
		}

		for(i = network->layers[0].count; i < network->neurons_total; i += 1) // skip input neurons
			gradient[i] /= samples_count;

		// Update weights and biases.
		for(layer = 1; layer < network->layers_count; layer += 1)
		{
			if (network->layers[layer].kernel_size != network->layers[layer - 1].count) // layer is convolution
			{
				size_t input_width = (network->layers[layer].columns + network->layers[layer].width - 1) * network->layers[layer].depth;
				size_t input_height = (network->layers[layer].rows + network->layers[layer].height - 1);
				size_t kernel_columns = network->layers[layer].depth * network->layers[layer].width;

				for(unit = 0; unit < network->layers[layer - 1].count; unit += 1)
				{
					size_t in = index_neuron(network, layer - 1, unit);

					size_t offset_x = unit % input_width;
					size_t offset_y = unit / input_width;

					// Calculate the (inclusive) range of kernel weights used with the input unit.
					size_t x_from = max(0, (int)offset_x + (int)kernel_columns - (int)input_width);
					size_t x_to = min((int)kernel_columns - 1, (int)offset_x);
					size_t y_from = max(0, (int)offset_y + (int)network->layers[layer].height - (int)input_height);
					size_t y_to = min((int)network->layers[layer].height - 1, (int)offset_y);

					size_t kernel, kernel_x, kernel_y;

					for(kernel = 0; kernel < network->layers[layer].kernels_count; kernel += 1)
					{
						for(kernel_y = y_from; kernel_y <= y_to; kernel_y += 1)
						{
							for(kernel_x = x_from; kernel_x <= x_to; kernel_x += 1)
							{
								size_t weight = index_weight(network, layer, kernel, kernel_y * kernel_columns + kernel_x);
								size_t i = (offset_y - kernel_y) * network->layers[layer].columns + (offset_x - kernel_x);
								size_t out = index_neuron_convolution(network, layer, kernel, i);
								weights[weight] += gradient[out] * activations[s][in];
							}
						}
					}
				}

				for(unit = 0; unit < network->layers[layer].count; unit += 1)
				{
					size_t kernel = unit / (network->layers[layer].columns * network->layers[layer].rows);
					size_t bias = index_bias(network, layer, kernel);
					size_t out = index_neuron(network, layer, unit);
					biases[bias] += gradient[out];
				}
			}
			else
			{
				for(unit = 0; unit < network->layers[layer].count; unit += 1)
				{
					unsigned bias, weight;
					size_t w;

					out = index_neuron(network, layer, unit);
					bias = index_bias(network, layer, unit);

					//biases[bias] += gradient[out] + options->weight_decay * network->biases[bias];
					biases[bias] += gradient[out];

					for(w = 0; w < network->layers[layer - 1].count; w += 1)
					{
						weight = index_weight(network, layer, unit, w);
						weights[weight] += gradient[out] * activations[s][index_neuron(network, layer - 1, w)];
					}
				}
			}
		}
	}

	return (struct gradient){.weights = weights, .biases = biases};
}

// WARNING: Assumes activations are initialized.
void weights_update(const struct nn *restrict network, const struct nn_options *restrict options, struct gradient g)
{
	size_t i;

	// TODO do gradient norm clipping to scale down to some highest allowed value (not to normalize)

	// Clip gradients and update paramters.
	double max = 1; // clip gradient only if it is bigger than 1 in some direction // TODO should this depend on the number of weights?
	for(i = 0; i < network->weights_total; i += 1)
	{
		g.weights[i] += options->weight_decay * network->weights[i];
		if (fabs(g.weights[i]) > max)
			max = fabs(g.weights[i]);
	}
	for(i = 0; i < network->biases_total; i += 1)
		if (fabs(g.biases[i]) > max)
			max = fabs(g.biases[i]);

#if 0
	for(i = 0; i < network->weights_total; i += 1)
	{
		if (i < network->biases_total)
			printf("%f ", g.biases[i]);
		printf("%f | ", g.weights[i]);
	}
	putchar('\n');
	if (max > 1)
		printf("max weight change: %f\n", max);
#endif

	for(i = 0; i < network->weights_total; i += 1)
	{
		if (i < network->biases_total)
			network->biases[i] -= options->learn_rate * g.biases[i] / max;
		network->weights[i] -= options->learn_rate * g.weights[i] / max;
	}

#if 0
	extern int print_debug;

	if (print_debug)
	{
		double w = 0, b = 0;
		double wmin = INFINITY, wmax = -INFINITY;
		double bmin = INFINITY, bmax = -INFINITY;
		double w2 = 0, b2 = 0;

		//for(i = 0; i < network->neurons_total; i += 1)
		//	printf("%f ", activations[samples_count - 1][i]);
		for(i = 0; i < network->weights_total; i += 1)
		{
			if (i < network->biases_total)
			{
				b += network->biases[i];
				b2 += network->biases[i] * network->biases[i];
				if (b < bmin)
					bmin = b;
				if (b > bmax)
					bmax = b;
			}
			w += network->weights[i];
			w2 += network->weights[i] * network->weights[i];
			if (w < wmin)
				wmin = w;
			if (w > wmax)
				wmax = w;
		}
		printf(">> w mean %.3f stddev %.3f | [%.3f %.3f]\n", w / network->weights_total, sqrt(w2 / network->weights_total - w * w / (network->weights_total * network->weights_total)), wmin, wmax);
		printf(">> b mean %.3f stddev %.3f | [%.3f %.3f]\n", b / network->biases_total, sqrt(b2 / network->biases_total - b * b / (network->biases_total * network->biases_total)), bmin, bmax);
		putchar('\n');
	}
#endif
}

// WARNING: Assumes activations are calculated.
double loss(const struct nn *restrict network, size_t samples_count, struct vector *restrict y, double *restrict *restrict activations)
{
	size_t s, p;
	double e = 0;

	for(s = 0; s < samples_count; s += 1)
	{
		unsigned layer = network->layers_count - 1;

		for(p = 0; p < network->layers[layer].count; p += 1)
		{
			size_t out = index_neuron(network, layer, p);
			double diff = activations[s][out] - *vector_get(y, s, p);
			e += diff * diff;
		}
		//e += loss_crossentropy(network, y, activations[s]);
	}
	e /= samples_count;

	return e;
}

double loss_regularization(const struct nn *restrict network, double decay_factor)
{
	size_t i;
	double e = 0;

	for(i = 0; i < network->weights_total; i += 1)
		e += network->weights[i] * network->weights[i];
	e *= decay_factor;

	return e;
}

void print_nn(const struct nn *restrict network)
{
	size_t k;
	for(k = 0; k < network->weights_total; k += 1)
		printf("%.4f ", network->weights[k]);
	printf("|");
	for(k = 0; k < network->biases_total; k += 1)
		printf(" %.4f", network->biases[k]);
	printf("\n");
}
