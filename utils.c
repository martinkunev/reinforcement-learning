#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "utils.h"

int argbest_random(double (*best)(double, double), const double *restrict scores, size_t scores_count)
{
	size_t i;
	int count, skip;
	double score_best;

	assert(scores_count >= 1);

	score_best = scores[0];
	count = 1;

	for(i = 1; i < scores_count; i += 1)
	{
		double score = scores[i];
		if (score == score_best)
			count += 1;
		else if (best(score, score_best) == score)
		{
			score_best = score;
			count = 1;
		}
	}

	skip = rand() % count;
	for(i = 0; i < scores_count; i += 1)
		if (scores[i] == score_best)
		{
			if (!skip)
				return (int)i;
			skip -= 1;
		}

	assert(0);
}

// Returns the index of a action at random with probability proportional to the softmax value of the action.
int argsoftmax_random(double (*best)(double, double), const double *restrict scores, size_t scores_count)
{
	size_t i;
	double total = 0, choice;

	assert(scores_count >= 1);

	for(i = 0; i < scores_count; i += 1)
		total += exp(scores[i]);

	choice = rand() / (double)RAND_MAX;

	for(i = 0; i < scores_count; i += 1)
	{
		double probability = exp(scores[i]) / total;
		choice -= probability;
		if (choice < 0)
			return (int)i;
	}

	// This can be reached due to limited floating point precision.
	return rand() % (int)scores_count;
}

void *memdup(const void *data, size_t size)
{
	void *result = malloc(size);
	if (!result)
		return 0;
	memcpy(result, data, size);
	return result;
}
