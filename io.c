#include <assert.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "vector.h"
#include "nn.h"
#include "io.h"

#define FACTOR -0.1
#define SAMPLES_COUNT 128

#define EXPORT_COUNT 0

#define LEARN_STEPS 100
#define LEARN_STAGE 10
#define LEARN_FACTOR_UPDATE 0.98

#define HIDDEN 128

#define PLOT(step, error) fprintf(stderr, "(%u, %f),\n", (unsigned)(step), (double)(error))

// https://deepai.org/dataset/mnist

// enum {ERROR_MEMORY = -1, ERROR_MISSING = -2, ERROR_INPUT = -3};

struct slice
{
	unsigned char *data;
	size_t count;
};

#define slice(d, c) (struct slice){.data = (d), .count = (c)}

struct image
{
	uint32_t magic;
	uint32_t count;
	uint32_t height;
	uint32_t width;
	unsigned char pixels[];
};

struct label
{
	uint32_t magic;
	uint32_t count;
	unsigned char labels[];
};

static struct slice file_open(const char *restrict path)
{
    int file;
    struct stat info;
    unsigned char *buffer;

    // Read file content.
    file = open(path, O_RDONLY);
    if (file < 0)
		return (struct slice){0}; // TODO this could be ERROR_ACCESS or something else
    if (fstat(file, &info) < 0)
    {
        close(file);
        return (struct slice){0}; // TODO this could be ERROR_ACCESS or something else
    }
    buffer = mmap(0, (size_t)info.st_size, PROT_READ, MAP_SHARED, file, 0);
    close(file);
    if (buffer == MAP_FAILED)
		return (struct slice){0};

	return slice(buffer, (size_t)info.st_size);
}

static void file_close(struct slice file)
{
    munmap(file.data, file.count);
}

void print_nn(const struct nn *restrict network);

struct nn *nn_load_file(const char *restrict filename)
{
	struct slice f = file_open(filename);
	struct nn *network;
	if (!f.data)
		return 0;
	network = nn_load(strchr((const char *)f.data, '\n') + 1); // first line is a comment, skip it
	file_close(f);
	return network;
}

int nn_dump_file(struct nn *restrict network, const char *restrict prefix, const char *restrict comment)
{
	// TODO this function is unsafe (int overflow, buffer overflow, file access concurrency)

	char buffer[256];
	unsigned i = 0;
	do
	{
		snprintf(buffer, sizeof(buffer), "%s_%u", prefix, i);
		i += 1;
	} while (access(buffer, F_OK) == 0);

	nn_dump(buffer, network, comment);
	printf("output: %s\n", buffer);
	return 0;
}

//print_nn(&network);
//	nn_term(&network);
