#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include "nn.h"
#include "utils.h"
#include "game.h"

struct interval
{
	double bound[2];
};

static double (*best[])(double, double) = {max, min};

void ai_utility(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	const struct nn *ai = argument;
	const double *output;
	size_t i;

	// TODO take a buffer from somewhere else
	double activations[INPUT_SIZE + OUTPUT_SIZE + 4096];

	assert(sizeof(activations) / sizeof(*activations) >= ai->neurons_total);

	input_set(activations, &game->state, 0);
	output = nn_regression(ai, activations);

	for(i = 0; i < game->moves_count; i += 1)
	{
		double value = output[action_index(game->moves[i])];
		if (game->state.player)
			value = -value;
		scores[i] = value;
	}
}

// WARNING: The matching between the indices of best and bounds assumes that player 0 is max and player 1 is min.
// TODO can this optimize: https://en.wikipedia.org/wiki/Negamax#Negamax_with_alpha_beta_pruning
static void minimax(const struct reversi *restrict game, struct interval bounds, unsigned lookahead, const struct player *restrict player, struct score *restrict score, int explore)
{
	int i;
	double scores[OUTPUT_SIZE];

	// TODO apply DISCOUNT_FACTOR on the score

	if (!game->moves_count)
	{
		score->value = game_score(&game->state);
		return; // WARNING: Assuming the caller is minimax (otherwise action remains uninitialized).
	}

	if (explore || !lookahead)
	{
		(*player->utility)(game, scores, player->network);

		if (explore)
		{
			// TODO optimization: calculate only score of the best move?

			i = rand() % (int)game->moves_count;
			*score = (struct score){.action = game->moves[i], .value = scores[i]};
			return;
		}
	}
	else
	{
		struct interval bounds_child = bounds;

		for(i = 0; i < game->moves_count; i += 1)
		{
			struct reversi *next = transition_alloc(game, game->moves[i]);
			minimax(next, bounds_child, lookahead - 1, player, score, 0);
			free(next);

			// Alpha-Beta pruning:
			// Cancel calculation if the game state is unreachable under optimal play of the other player.
			if ((*best[1 - game->state.player])(bounds.bound[1 - game->state.player], score->value) != score->value)
				return;

			bounds_child.bound[game->state.player] = (*best[game->state.player])(bounds_child.bound[game->state.player], score->value);
			scores[i] = score->value;
		}
	}

	/*if (game->state.player == 1)
		for(i = 0; i < game->moves_count; i += 1)
			printf("(%u, %u) %.1f\n", (unsigned)game->moves[i].y, (unsigned)game->moves[i].x, scores[i]);*/

	i = argbest_random(best[game->state.player], scores, game->moves_count);
	//i = argsoftmax_random(best[game->state.player], scores, game->moves_count);
	*score = (struct score){.action = game->moves[i], .value = scores[i]};
}

// Used for random play.
void zero_utility(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;
	for(i = 0; i < game->moves_count; i += 1)
		scores[i] = 0;
}

struct score bot_play(const struct reversi *restrict game, const struct player *const restrict *restrict players, double explore_rate)
{
	struct score score;
	minimax(game, (struct interval){-INFINITY, +INFINITY}, players[game->state.player]->lookahead, players[game->state.player], &score, rand_bernoulli(explore_rate));
	return score;
}

// Returns the utility of state under the greedy policy specified by nn.
// TODO use minimax?
double ai_score(struct nn *restrict nn, const struct state *restrict state)
{
	struct reversi game;
	double scores[OUTPUT_SIZE];
	int i;

	// TODO we copy the state and calculate neighbors; this is inefficient
	game.state = *state;
	neighbors(&game);

	assert(game.moves_count);

	// The highest output value of the network corresponds to the best action of the current player.
	ai_utility(&game, scores, nn);
	i = argbest_random(best[state->player], scores, game.moves_count);

	return scores[i];
}

struct position position_transform(struct position base, unsigned symmetry)
{
	struct position new;

	if (symmetry & TRANSFORM_SWITCH_XY)
		new.x = base.y, new.y = base.x;
	else
		new.x = base.x, new.y = base.y;
	if (symmetry & TRANSFORM_INVERSE_X)
		new.x = BOARD_SIZE - 1 - new.x;
	if (symmetry & TRANSFORM_INVERSE_Y)
		new.y = BOARD_SIZE - 1 - new.y;

	return new;
}

void input_set(double *restrict input, const struct state *restrict state, unsigned augment)
{
	size_t x, y;
	size_t i = 0;

	for(y = 0; y < BOARD_SIZE; y += 1)
	{
		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			unsigned char sign;

			// Augment data by applying a symmetry from the D4 group to the board.
			if (augment)
			{
				struct position new = position_transform((struct position){.y = y, .x = x}, augment);
				sign = state->board[new.y][new.x];
			}
			else
				sign = state->board[y][x];

			// 2 input neurons for each field (one for each player)
			// interchange inputs for x and o depending on current player
			// neuron 0: current player; neuron 1: other player

			switch (sign)
			{
			case ' ':
				input[i++] = 0;
				input[i++] = 0;
				break;

			case 'x':
				input[i++] = !state->player;
				input[i++] = state->player;
				break;

			case 'o':
				input[i++] = state->player;
				input[i++] = !state->player;
				break;
			}
		}
	}

	assert(i == INPUT_SIZE);

	// TODO is this harmful? is it necessary?
	//sample->input[i++] = state->player;
}

struct reversi *transition_alloc(const struct reversi *restrict game, struct position action)
{
	struct reversi *new = move_alloc(game, action);
	if (!new)
		return 0;

	new->state.player = 1 - game->state.player;
	new->state.time = game->state.time + 1;

	neighbors(new);
	if (!new->moves_count)
	{
		new->state.player = 1 - new->state.player;
		neighbors(new);
	}

	assert(new->state.player >= 0);
	return new;
}
