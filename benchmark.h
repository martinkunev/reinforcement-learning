struct nn;

struct agent_model
{
	struct nn *network;
	_Atomic double elo;

	_Atomic double vs_h1;
	unsigned episodes;
};

static inline int worse(struct agent_model a, struct agent_model b)
{
	double r0 = (double)a.vs_h1 / a.episodes;
	double r1 = (double)b.vs_h1 / b.episodes;
	return r0 < r1;
}

#define HEAP_GLOBAL
#define HEAP_TYPE struct agent_model
#define HEAP_ABOVE(a, b) ((a).elo < (b).elo)
//#define HEAP_ABOVE(a, b) worse((a), (b))
#include "heap.g"

enum {HISTORY_CAPACITY = 32};

struct optimal
{
	struct agent_model agents[AGENTS_COUNT + 1 + HISTORY_CAPACITY];
	struct heap top;
	struct nn *best;

	struct agent_model agents_fixed[3];
};

void *main_test(void *argument);
int test_game(const struct player *restrict players, int player_first, double explore_rate_start, double explore_rate, int verbose);

#define test_game(p, f, s, e, ...) (test_game)((p), (f), (s), (e), __VA_ARGS__ + 0)

void print_progress(struct nn *restrict network);
