struct vector
{
	size_t shape[2];
	double buffer[];
};

struct vector *vector_alloc(size_t rows, size_t columns);
double *vector_get(struct vector *restrict vector, size_t row, size_t column);
