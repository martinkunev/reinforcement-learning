#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "game.h"

struct board_info
{
	int rows[BOARD_SIZE];
	int columns[BOARD_SIZE];
	int diagonal_main[BOARD_SIZE * 2 - 1];
	int diagonal_other[BOARD_SIZE * 2 - 1];
};

#define DIAGONAL_MAIN(y, x) (BOARD_SIZE - 1 + (x) - (y))
#define DIAGONAL_OTHER(y, x) ((x) + (y))

struct reversi *game_alloc(void)
{
	struct reversi *new = malloc(sizeof(*new));
	if (!new)
		abort();

	assert(BOARD_SIZE == 6 || BOARD_SIZE == 8);

	memset(new->state.board, ' ', sizeof(new->state.board));
	new->state.player = 0;
	new->state.time = 0;

	new->state.board[BOARD_SIZE / 2 - 1][BOARD_SIZE / 2 - 1] = new->state.board[BOARD_SIZE / 2][BOARD_SIZE / 2] = 'x';
	new->state.board[BOARD_SIZE / 2 - 1][BOARD_SIZE / 2] = new->state.board[BOARD_SIZE / 2][BOARD_SIZE / 2 - 1] = 'o';

	neighbors(new);
	new->stones_difference = 0;

	return new;
}

static int flip(struct state *restrict state, struct line *restrict line, int from, int to)
{
	int i;
	for(i = from; i < to; i += 1)
		state->board[line->fields[i].y][line->fields[i].x] = player2sign(state->player);
	return to - from;
}

// Flip stones in a specified line and count the number of stones flipped.
static int flips(struct state *restrict state, struct line *restrict line, int line_index)
{
	int i;
	int count = 0;
	unsigned char stone;

	for(i = line_index - 1; i >= 0; i -= 1)
	{
		stone = state->board[line->fields[i].y][line->fields[i].x];
		if (stone == player2sign(state->player))
		{
			count += flip(state, line, i + 1, line_index);
			break;
		}
		else if (stone == ' ')
			break; // no own stone

		assert(stone == player2sign(1 - state->player));
	}

	for(i = line_index + 1; i < (int)line->count; i += 1)
	{
		stone = state->board[line->fields[i].y][line->fields[i].x];
		if (stone == player2sign(state->player))
		{
			count += flip(state, line, line_index + 1, i);
			break;
		}
		else if (stone == ' ')
			break; // no own stone

		assert(stone == player2sign(1 - state->player));
	}

	return count;
}

struct reversi *move_alloc(const struct reversi *restrict game, struct position action)
{
	int flipped;
	struct line line;
	int i;

	struct reversi *new = memdup(game, sizeof(*new));
	if (!new)
		abort();

	for(i = 0; i < BOARD_SIZE; i += 1)
		line.fields[i] = (struct position){action.y, i};
	line.count = BOARD_SIZE;
	flipped = flips(&new->state, &line, action.x);

	for(i = 0; i < BOARD_SIZE; i += 1)
		line.fields[i] = (struct position){i, action.x};
	flipped += flips(&new->state, &line, action.y);

	line.count = 0;
	for(i = 0; i < BOARD_SIZE; i += 1)
	{
		int index = action.x + (i - action.y);
		if ((0 <= index) && (index < BOARD_SIZE))
			line.fields[line.count++] = (struct position){i, index};
	}
	flipped += flips(&new->state, &line, action.y - line.fields[0].y);

	line.count = 0;
	for(i = 0; i < BOARD_SIZE; i += 1)
	{
		int index = action.x + (action.y - i);
		if ((0 <= index) && (index < BOARD_SIZE))
			line.fields[line.count++] = (struct position){i, index};
	}
	flipped += flips(&new->state, &line, action.y - line.fields[0].y);

	assert(flipped);

	new->state.board[action.y][action.x] = player2sign(game->state.player);
	new->state.time == 1;

	new->stones_difference += game->state.player ? -(flipped + 1) : (flipped + 1);

	return new;
}

void neighbors(struct reversi *restrict game)
{
	int y, x;

	// There are 2 * BOARD_SIZE - 1 diagonals.

	unsigned char rows[BOARD_SIZE] = {0}, cols[BOARD_SIZE] = {0};
	unsigned char diag_main[2 * BOARD_SIZE - 1] = {0}, diag_other[2 * BOARD_SIZE - 1] = {0};
	unsigned char opponent = player2sign(1 - game->state.player);

	memset(game->moves_board, 0, sizeof(game->moves_board));
	game->moves_count = 0;

	// Traverse board in each direction to determine which fields are playable.
	// A field is playable if there is an opponent stone in the previous field.

	// top-left -> bottom-right
	for(y = 0; y < BOARD_SIZE; y += 1)
	{
		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			int m = BOARD_SIZE - 1 + x - y;
			int o = x + y;

			if (game->state.board[y][x] == ' ')
			{
				if (!game->moves_board[y][x])
				{
					if (rows[y] && x && (game->state.board[y][x - 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (cols[x] && y && (game->state.board[y - 1][x] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (diag_main[m] && x && y && (game->state.board[y - 1][x - 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (diag_other[o] && (x < BOARD_SIZE - 1) && y && (game->state.board[y - 1][x + 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
				}
				rows[y] = 0;
				cols[x] = 0;
				diag_main[m] = 0;
				diag_other[o] = 0;
			}
			else if (game->state.board[y][x] != opponent)
			{
				rows[y] = 1;
				cols[x] = 1;
				diag_main[m] = 1;
				diag_other[o] = 1;
			}
		}
	}

	// bottom-right -> top-left
	for(y = BOARD_SIZE - 1; y >= 0; y -= 1)
	{
		for(x = BOARD_SIZE - 1; x >= 0; x -= 1)
		{
			int m = BOARD_SIZE - 1 + x - y;
			int o = x + y;

			if (game->state.board[y][x] == ' ')
			{
				if (!game->moves_board[y][x])
				{
					if (rows[y] && (x < BOARD_SIZE - 1) && (game->state.board[y][x + 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (cols[x] && (y < BOARD_SIZE - 1) && (game->state.board[y + 1][x] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (diag_main[m] && (x < BOARD_SIZE - 1) && (y < BOARD_SIZE - 1) && (game->state.board[y + 1][x + 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
					else if (diag_other[o] && x && (y < BOARD_SIZE - 1) && (game->state.board[y + 1][x - 1] == opponent))
					{
						game->moves_board[y][x] = 1;
						game->moves[game->moves_count++] = (struct position){y, x};
					}
				}
				rows[y] = 0;
				cols[x] = 0;
				diag_main[m] = 0;
				diag_other[o] = 0;
			}
			else if (game->state.board[y][x] != opponent)
			{
				rows[y] = 1;
				cols[x] = 1;
				diag_main[m] = 1;
				diag_other[o] = 1;
			}
		}
	}
}

// TODO distinguish finished from non-finished game in this function
int game_score(const struct state *restrict state)
{
	size_t x, y;
	static const int value[] = {['x'] = 1, ['o'] = -1};
	int total = 0;
	for(y = 0; y < BOARD_SIZE; y += 1)
		for(x = 0; x < BOARD_SIZE; x += 1)
			total += value[state->board[y][x]];
	return total;
}

/*double heuristic(const struct reversi *restrict game, void *restrict argument)
{
	size_t x, y, i;

	double score = 0;

	static const struct position offsets[] = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

	for(y = 0; y < BOARD_SIZE; y += 1)
	{
		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			unsigned counts[] = {0, 0, 0};
			unsigned walls = 0;

			double c = 1 - 4.0 * game->state.time / (3 * BOARD_SIZE * BOARD_SIZE);
			if (c < 0)
				c = 0;

			// Count stones on neighboring squares.
			for(i = 0; i < sizeof(offsets) / sizeof(*offsets); i += 1)
			{
				struct position position = {y + offsets[i].y, x + offsets[i].x};
				if ((position.y < 0) || (position.y >= BOARD_SIZE) || (position.x < 0) || (position.x >= BOARD_SIZE))
					walls += 1; // out of the board
				else
					counts[sign2player(game->state.board[position.y][position.x])] += 1;
			}

			if (game->state.board[y][x] == player2sign(game->state.player))
				score += 1 + c * (counts[1 - game->state.player] + walls * 2);
			else if (game->state.board[y][x] == player2sign(1 - game->state.player))
				score -= 1 + c * (counts[game->state.player] + walls * 2);
		}
	}

	return score + game->moves_count;
}*/

void heuristic_utility0(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;

	for(i = 0; i < game->moves_count; i += 1)
	{
		struct position action = game->moves[i];
		struct reversi *next = transition_alloc(game, action);
		scores[i] = (next->state.player == game->state.player) ? 0 : -(double)next->moves_count;
		free(next);
	}
}

static int is_fixed(unsigned char fixed[BOARD_SIZE][BOARD_SIZE], const struct state *restrict state, const struct board_info *restrict blank, int y, int x)
{
	int fixed_horizontal, fixed_vertical, fixed_diag_main, fixed_diag_other;
	int diagonal_border = (x == 0) || (x == BOARD_SIZE - 1) || (y == 0) || (y == BOARD_SIZE - 1);

	if (state->board[y][x] == ' ')
		return 0;

	fixed_horizontal = (x == 0) || (x == BOARD_SIZE - 1) || !blank->rows[y] || ((state->board[y][x] == state->board[y][x - 1]) && fixed[y][x - 1]) || ((state->board[y][x] == state->board[y][x + 1]) && fixed[y][x + 1]);
	fixed_vertical = (y == 0) || (y == BOARD_SIZE - 1) || !blank->columns[x] || ((state->board[y][x] == state->board[y - 1][x]) && fixed[y - 1][x]) || ((state->board[y][x] == state->board[y + 1][x]) && fixed[y + 1][x]);
	fixed_diag_main = diagonal_border || !blank->diagonal_main[DIAGONAL_MAIN(y, x)] || ((state->board[y][x] == state->board[y - 1][x - 1]) && fixed[y - 1][x - 1]) || ((state->board[y][x] == state->board[y + 1][x + 1]) && fixed[y + 1][x + 1]);
	fixed_diag_other = diagonal_border || !blank->diagonal_other[DIAGONAL_OTHER(y, x)] || ((state->board[y][x] == state->board[y - 1][x + 1]) && fixed[y - 1][x + 1]) || ((state->board[y][x] == state->board[y + 1][x - 1]) && fixed[y + 1][x - 1]);
	return fixed_horizontal && fixed_vertical && fixed_diag_main && fixed_diag_other;
}

static int fill_fixed(unsigned char fixed[BOARD_SIZE][BOARD_SIZE], const struct state *restrict state, const struct board_info *restrict blank, int y_init, int y_step, int y_limit, int x_init, int x_step, int x_limit)
{
	int limit = x_limit;
	int filled = 0;
	int y, x;
	for(y = y_init; y != y_limit; y += y_step)
	{
		for(x = x_init; x != limit; x += x_step)
		{
			if (is_fixed(fixed, state, blank, y, x))
			{
				fixed[y][x] = 1;
				filled += state->player ? 1 : -1;
			}
			else
			{
				limit = x;
				if (x_init == limit)
					break;
				else
					return filled;
			}
		}
	}
	return filled;
}

static void board_info_update(struct board_info *restrict info, int y, int x, int value)
{
	info->rows[y] += value;
	info->columns[x] += value;
	info->diagonal_main[DIAGONAL_MAIN(y, x)] += value;
	info->diagonal_other[DIAGONAL_OTHER(y, x)] += value;
}

void heuristic_utility1(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;
	int x, y;

	// Score is determined by:
	// * points for putting a stone on a given square
	// * 1 point per stone whose ownership cannot be changed (false negatives are possible due to multi-step changes)
	// * 1 point lost for each movement option available to the opponent
	// * points for the fraction of stones on the board the player owns

	double points[BOARD_SIZE][BOARD_SIZE] =
	{
		{16, -5, 4, 4, -5, 16},
		{-5, -16, 0, 0, -16, -5},
		{4, 0, 1, 1, 0, 4},
		{4, 0, 1, 1, 0, 4},
		{-5, -16, 0, 0, -16, -5},
		{16, -5, 4, 4, -5, 16},
	};
	// WARNING: Assumes 8x8 board.
	/*double points[BOARD_SIZE][BOARD_SIZE] =
	{
		{16, -5, 4, 2, 2, 4, -5, 16},
		{-5, -16, 0, 0, 0, 0, -16, -5},
		{4, 0, 1, 2, 2, 1, 0, 4},
		{2, 0, 2, 3, 3, 2, 0, 2},
		{2, 0, 2, 3, 3, 2, 0, 2},
		{4, 0, 1, 2, 2, 1, 0, 4},
		{-5, -16, 0, 0, 0, 0, -16, -5},
		{16, -5, 4, 2, 2, 4, -5, 16},
	};*/

	struct board_info blank = {0};

	// Adjust points for taken corners.
	if (game->state.board[0][0] != ' ')
		points[1][1] = points[2][0] = points[1][0] = points[0][2] = points[0][1] = 1;
	if (game->state.board[0][BOARD_SIZE - 1] != ' ')
		points[1][BOARD_SIZE - 2] = points[2][BOARD_SIZE - 1] = points[1][BOARD_SIZE - 1] = points[0][BOARD_SIZE - 3] = points[0][BOARD_SIZE - 2] = 1;
	if (game->state.board[BOARD_SIZE - 1][0] != ' ')
		points[BOARD_SIZE - 2][1] = points[BOARD_SIZE - 3][0] = points[BOARD_SIZE - 2][0] = points[BOARD_SIZE - 1][2] = points[BOARD_SIZE - 1][1] = 1;
	if (game->state.board[BOARD_SIZE - 1][BOARD_SIZE - 1] != ' ')
		points[BOARD_SIZE - 2][BOARD_SIZE - 2] = points[BOARD_SIZE - 3][BOARD_SIZE - 1] = points[BOARD_SIZE - 2][BOARD_SIZE - 1] = points[BOARD_SIZE - 1][BOARD_SIZE - 3] = points[BOARD_SIZE - 1][BOARD_SIZE - 2] = 1;

	for(y = 0; y < BOARD_SIZE; y += 1)
		for(x = 0; x < BOARD_SIZE; x += 1)
			if (game->state.board[y][x] == ' ')
				board_info_update(&blank, y, x, 1);

	for(i = 0; i < game->moves_count; i += 1)
	{
		unsigned char fixed[BOARD_SIZE][BOARD_SIZE] = {0};

		struct position action = game->moves[i];
		struct reversi *next = transition_alloc(game, action);
		double score = 32.0 * (game->state.player ? -next->stones_difference : next->stones_difference) / (next->state.time + 4);

		//score += (double)(game->state.player ? -next->stones_difference : next->stones_difference) * next->state.time / (BOARD_SIZE * BOARD_SIZE);

		if (next->state.player != game->state.player)
			score -= next->moves_count;

		free(next);

		score += points[action.y][action.x];

		// Use blank counts to determine which stones are fixed (cannot be flipped).
		// Flood-fill from top-left, top-right, bottom-left and bottom-right.
		board_info_update(&blank, action.y, action.x, -1);
		score += fill_fixed(fixed, &game->state, &blank, 0, 1, BOARD_SIZE, 0, 1, BOARD_SIZE);
		score += fill_fixed(fixed, &game->state, &blank, 0, 1, BOARD_SIZE, BOARD_SIZE - 1, -1, -1);
		score += fill_fixed(fixed, &game->state, &blank, BOARD_SIZE - 1, -1, -1, 0, 1, BOARD_SIZE);
		score += fill_fixed(fixed, &game->state, &blank, BOARD_SIZE - 1, -1, -1, BOARD_SIZE - 1, -1, -1);
		board_info_update(&blank, action.y, action.x, 1);

		scores[i] = game->state.player ? -score : score;
		//scores[i] = score;
	}
}
