#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "game.h"

// TODO distinguish finished from non-finished game in this function
int game_score(const struct state *restrict state)
{
	size_t i;
	static const int value[] = {['x'] = 1, ['o'] = -1};

	for(i = 0; i < BOARD_SIZE; i += 1)
	{
		// check row
		if ((state->board[i][0] != ' ') && (state->board[i][1] == state->board[i][0]) && (state->board[i][2] == state->board[i][0]))
			return value[state->board[i][0]];

		// check column
		if ((state->board[0][i] != ' ') && (state->board[1][i] == state->board[0][i]) && (state->board[2][i] == state->board[0][i]))
			return value[state->board[0][i]];
	}

	// check main diagonal
	if ((state->board[1][1] != ' ') && (state->board[0][0] == state->board[1][1]) && (state->board[2][2] == state->board[1][1]))
		return value[state->board[1][1]];

	// check other diagonal
	if ((state->board[1][1] != ' ') && (state->board[0][2] == state->board[1][1]) && (state->board[2][0] == state->board[1][1]))
		return value[state->board[1][1]];

	return 0;
}

struct reversi *move_alloc(const struct reversi *restrict game, struct position action)
{
	struct reversi *new = memdup(game, sizeof(*new));
	if (!new)
		abort();
	new->state.board[action.y][action.x] = player2sign(game->state.player);
	new->state.time == 1;
	return new;
}

void neighbors(struct reversi *restrict game)
{
	int y, x;

	memset(game->moves_board, 0, sizeof(game->moves_board));
	game->moves_count = 0;

	if (game_score(&game->state))
		return;

	for(y = 0; y < BOARD_SIZE; y += 1)
	{
		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			if (game->state.board[y][x] == ' ')
			{
				game->moves_board[y][x] = 1;
				game->moves[game->moves_count++] = (struct position){y, x};
			}
		}
	}
}

struct reversi *game_alloc(void)
{
	struct reversi *new = malloc(sizeof(*new));
	if (!new)
		abort();

	assert(BOARD_SIZE == 3);

	memset(new->state.board, ' ', sizeof(new->state.board));
	new->state.player = 0;
	new->state.time = 0;

	neighbors(new);

	return new;
}

/*static double score_real(struct state *restrict state)
{
	size_t x, y;

	double s = game_score(state), s_new;
	if (s || (state->time == OUTPUT_SIZE))
		return s;

	// Bruteforce.
	s = (*best[1 - state->player])(1, -1);
	for(y = 0; y < BOARD_SIZE; y += 1)
		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			if (state->board[y][x] != ' ')
				continue;

			state->board[y][x] = player2sign(state->player);
			state->player = 1 - state->player;
			state->time += 1;

			s_new = score_real(state);

			state->time -= 1;
			state->player = 1 - state->player;
			state->board[y][x] = ' ';

			s = (*best[state->player])(s, s_new);
		}
	return s;
}*/

void heuristic_utility1(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;
	double s;

	unsigned char self = player2sign(game->state.player), opponent = player2sign(1 - game->state.player);

	unsigned points_self[] = {0, 2, 32};
	unsigned points_opponent[] = {0, 2, 6};

	// Check if the current player can win in 1 move.
	for(i = 0; i < game->moves_count; i += 1)
	{
		struct position move = game->moves[i];
		((struct reversi *)game)->state.board[move.y][move.x] = player2sign(game->state.player);
		s = game_score(&game->state);
		((struct reversi *)game)->state.board[move.y][move.x] = ' ';

		if (s) // end game means win for the current player
			scores[i] = 64;
		else
		{
			scores[i] = 0;
			if ((move.x == 1) && (move.y == 1)) // center
				scores[i] += 31;
			else
			{
				int value_self, value_opponent;

				value_self = ((game->state.board[0][move.x] == self) + (game->state.board[1][move.x] == self) + (game->state.board[2][move.x] == self));
				value_opponent = ((game->state.board[0][move.x] == opponent) + (game->state.board[1][move.x] == opponent) + (game->state.board[2][move.x] == opponent));
				if (!value_self != !value_opponent) // logical XOR
					scores[i] += points_self[value_self] + points_opponent[value_opponent];

				value_self = ((game->state.board[move.y][0] == self) + (game->state.board[move.y][1] == self) + (game->state.board[move.y][2] == self));
				value_opponent = ((game->state.board[move.y][0] == opponent) + (game->state.board[move.y][1] == opponent) + (game->state.board[move.y][2] == opponent));
				if (!value_self != !value_opponent) // logical XOR
					scores[i] += points_self[value_self] + points_opponent[value_opponent];

				if ((move.x != 1) && (move.y != 1)) // angle
				{
					value_self = ((game->state.board[2 - move.y][2 - move.x] == self) + (game->state.board[1][1] == self));
					value_opponent = ((game->state.board[2 - move.y][2 - move.x] == opponent) + (game->state.board[1][1] == opponent));

					/*if ((scores[i] == 4) && (value_self == 1))
					{
						// special case to handle "  x" " o " "x  "
						scores[i] -= 1;
						continue;
					}*/

					if (!value_self != !value_opponent) // logical XOR
						scores[i] += points_self[value_self] + points_opponent[value_opponent];

					scores[i] += 1;
				}
			}
		}

		if (game->state.player)
			scores[i] = -scores[i];
	}

	assert((game->state.time > 0) || (game->moves_count == OUTPUT_SIZE));

	/*putchar('>');
	for(i = 0; i < game->moves_count; i += 1)
		printf(" (%u,%u)%f", game->moves[i].y, game->moves[i].x, scores[i]);
	putchar('\n');
	fflush(stdout);*/
}

void heuristic_utility0(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;
	double s;

	// Check if the current player can win in 1 move.
	for(i = 0; i < game->moves_count; i += 1)
	{
		struct position move = game->moves[i];
		((struct reversi *)game)->state.board[move.y][move.x] = player2sign(game->state.player);
		s = game_score(&game->state);
		((struct reversi *)game)->state.board[move.y][move.x] = ' ';
		scores[i] = (s != 0); // end game means win for the current player

		if (game->state.player)
			scores[i] = -scores[i];
	}

	assert((game->state.time > 0) || (game->moves_count == OUTPUT_SIZE));
}
