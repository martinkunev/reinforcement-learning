#define _GNU_SOURCE

#include <assert.h>
#include <ctype.h>
#include <fenv.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "vector.h"
#include "nn.h"
#include "utils.h"
#include "game.h"
#include "train.h"
#include "io.h"
#include "benchmark.h"

//#define PERFORMANCE 1

enum {GENERATION = 512, GAMES_COUNT = GENERATION * 8192};

#define EXPLORE_RATE (double)1.0

#define FACTOR ((double)0.01)
//#define FACTOR ((double)0.05)

//#define LEARN_UPDATE 0.99
//#define LEARN_UPDATE 0.995
#define LEARN_UPDATE ((double)1.0)

enum {TOURNAMENT_GAMES = 4096};

#define S(s) (s), sizeof(s) - 1

//#define DEBUG_Q

// Designed for 2-player zero-sum games which can be modelled as MDPs (full observability).
// Q-learning (one-step off-policy TD(0) learning with semi-gradient descent).

int print_debug;

/*
ideas:
reducing learning rate over time may cause issues for a changing policy
x is priliveged in the learning (nn is updated more often for x)
why is o winning more? o usually has parity (playing last)

a known heuristic:
https://github.com/arminkz/Reversi#heuristic-functions
https://kartikkukreja.wordpress.com/2013/03/30/heuristic-function-for-reversiothello/
*/

void print(const struct reversi *restrict game)
{
	size_t i, j;
	const char *letters = "ABCDEFGH";
	printf("  %.*s\n", BOARD_SIZE, letters);
	for(i = 0; i < BOARD_SIZE; i += 1)
	{
		printf("%zu ", i + 1);
		for(j = 0; j < BOARD_SIZE; j += 1)
			putchar(game->state.board[i][j]);
		printf("|\n");
	}
	putchar(' ');
	putchar(' ');
	for(i = 0; i < BOARD_SIZE; i += 1)
		putchar('-');
	putchar('\n');
	putchar('\n');
}

static struct score human_play(const struct reversi *restrict game, const struct player *const restrict *restrict player, double explore_rate)
{
	struct position action;
	print(game);
	do
	{
		char a, b;
		static const int letters[256] = {['a'] = 1, ['b'] = 2, ['c'] = 3, ['d'] = 4, ['e'] = 5, ['f'] = 6, ['g'] = 7, ['h'] = 8};
		int digit, letter;
		int scanned;

		printf("move: ");
		scanned = scanf(" %c %c", &a, &b);
		if (scanned == EOF)
			exit(0);
		if (scanned != 2)
		{
			while (getchar() != '\n')
				;
			continue;
		}

		if (isdigit(a))
		{
			digit = a - '0';
			letter = letters[tolower(b)];
		}
		else if (isdigit(b))
		{
			digit = b - '0';
			letter = letters[tolower(a)];
		}
		else
			continue;
		if (!letter)
			continue;

		action = (struct position){digit - 1, letter - 1};
	} while (!game->moves_board[action.y][action.x]);
	return (struct score){.action = action};
}

static void human_init(struct player *restrict player)
{
	player->play = &human_play;
	player->utility = &heuristic_utility1; // used by minimax
	player->lookahead = 0;
	player->id = "<human>";
}

static void random_init(struct player *restrict player)
{
	player->play = &bot_play;
	player->utility = &zero_utility;
	player->lookahead = 0;
	player->id = "<random>";
}

static void heuristic_init(struct player *restrict player)
{
	player->play = &bot_play;
	player->utility = &heuristic_utility0;
	player->lookahead = 0;
	player->id = "<heuristic>";
}

static int rl_init(struct player *restrict player, const char *restrict player_type)
{
	if (!player_type || !*player_type)
	{
		// TODO having consecutive convolutional layers makes a mess
		// the second convolutional layer doesn't know to ignore the kernels dimension from the previous one
		// the order of the dimensions is also messy

		// Initialize a new network.
		if (BOARD_SIZE == 3) // tictactoe
		{
			static const struct layer layers[] =
			{
				{LAYER_INPUT, 0, INPUT_SIZE},
				//{LAYER_DENSE, ACTIVATION_RELU, 16},
				{LAYER_CONVOLUTION, ACTIVATION_RELU, 25, 1, 1, 2, 3, 3},
				{LAYER_DENSE, ACTIVATION_IDENTITY, OUTPUT_SIZE},
			};
			player->network = nn_init(layers, sizeof(layers) / sizeof(*layers));
		}
		else if (BOARD_SIZE == 6) // reversi
		{
			static const struct layer layers[] =
			{
				{LAYER_INPUT, 0, INPUT_SIZE},
				//{LAYER_DENSE, ACTIVATION_RELU, 128},
				{LAYER_CONVOLUTION, ACTIVATION_RELU, 24, BOARD_SIZE - 2, BOARD_SIZE - 2, 2, 3, 3},
				{LAYER_DENSE, ACTIVATION_IDENTITY, OUTPUT_SIZE},
			};
			player->network = nn_init(layers, sizeof(layers) / sizeof(*layers));
		}
		else if (BOARD_SIZE == 7) // four
		{
			static const struct layer layers[] =
			{
				{LAYER_INPUT, 0, INPUT_SIZE},
				{LAYER_CONVOLUTION, ACTIVATION_RELU, 25, BOARD_SIZE - 2, BOARD_SIZE - 2, 2, 3, 3},
				{LAYER_DENSE, ACTIVATION_IDENTITY, OUTPUT_SIZE},
			};
			player->network = nn_init(layers, sizeof(layers) / sizeof(*layers));
		}
		else if (BOARD_SIZE == 8) // reversi
		{
			static const struct layer layers[] =
			{
				{LAYER_INPUT, 0, INPUT_SIZE},
				{LAYER_CONVOLUTION, ACTIVATION_RELU, 64, BOARD_SIZE - 4, BOARD_SIZE - 4, 2, 5, 5},
				{LAYER_DENSE, ACTIVATION_IDENTITY, OUTPUT_SIZE},
			};
			player->network = nn_init(layers, sizeof(layers) / sizeof(*layers));
		}
		if (!player->network)
			return -1;
	}
	else if ((player_type[0] == '+') && isdigit(player_type[1]) && !player_type[2])
	{
		player->play = &bot_play;
		player->utility = &heuristic_utility1;
		player->lookahead = (unsigned)(player_type[1] - '0');
		player->id = player_type;

		return 0;
	}
	else if ((player_type[0] == '=') && isdigit(player_type[1]) && !player_type[2])
	{
		player->play = &bot_play;
		player->utility = &zero_utility;
		player->lookahead = (unsigned)(player_type[1] - '0');
		player->id = player_type;

		return 0;
	}
	else
	{
		// Load a trained network from a file.
		player->network = nn_load_file(player_type);
		if (!player->network)
			return -1;
	}

	player->play = &bot_play;
	player->utility = &ai_utility;
	player->lookahead = 0;
	player->id = player_type ? player_type : "<nn>";

	return 0;
}

static void test_tournament(struct player *restrict players, size_t games_count)
{
	unsigned wins[2][2] = {0}; // rows correspond to players; column index corresponds to starting first
	unsigned draws = 0;
	int total = 0;
	size_t i;

	// To measure performance accurately, always play equal number of games where each player starts.
	assert(games_count % 2 == 0);

	for(i = 0; i < games_count; i += 1)
	{
		int first = i % 2;
		int score = test_game(players, first, 1, 0);

		switch (sign(score))
		{
		case 1:
			wins[0][first == 0] += 1;
			break;

		case -1:
			wins[1][first == 1] += 1;
			break;

		case 0:
			draws += 1;
			break;

		default:
			assert(0);
		}
		total += score;
	}

	printf("%s: \tF %.3f S %.3f total %.3f\n", players[0].id, 2.0 * wins[0][1] / games_count, 2.0 * wins[0][0] / games_count, (double)(wins[0][0] + wins[0][1]) / games_count);
	printf("%s: \tF %.3f S %.3f total %.3f\n", players[1].id, 2.0 * wins[1][1] / games_count, 2.0 * wins[1][0] / games_count, (double)(wins[1][0] + wins[1][1]) / games_count);
	printf("draw: \t%.3f\naverage (%s): %.3f\n", (double)draws / games_count, players[0].id, (double)total / games_count);

	if (players[0].network)
		print_progress(players[0].network);
}

// Returns whether to play an interactive game.
static int startup(int argc, char *argv[], struct player *restrict players)
{
	size_t i;
	int human = -1;
	const char *network = 0, *opponent = 0;

	for(i = 1; i < argc; i += 1)
	{
		if ((argv[i][0] == '-') && argv[i][1] && !argv[i][2])
		{
			if (i + 1 >= argc)
				goto usage; // missing argument after the option

			switch (argv[i++][1])
			{
			case 'i': // interactive
				if (argv[i][1])
					goto usage; // invalid player
				human = sign2player(argv[i][0]);
				if ((human != 0) && (human != 1))
					goto usage; // invalid player
				break;

			case 'n': // startup neural network
				network = argv[i];
				break;

			case 't': // specify test opponent
				opponent = argv[i];
				break;

			case 's': // random seed
				srand(strtol(argv[i], 0, 10));
				break;

			default:
				goto usage;
			}
		}
		else
			goto usage;
	}

	if (human >= 0)
	{
		if (opponent)
			fprintf(stderr, "option -t %s ignored\n", opponent);
		human_init(players + human);
		if (rl_init(players + 1 - human, network) < 0)
			abort();
		if (players[1 - human].utility == &ai_utility)
			players[1 - human].lookahead = 3; // make opponent more challenging
		return 1;
	}
	else
	{
		if (rl_init(players, network) < 0)
			abort();
		if (opponent)
		{
			if (rl_init(players + 1, opponent) < 0)
				abort();
			test_tournament(players, TOURNAMENT_GAMES);
			exit(0);
		}
		return 0;
	}

usage:
	fprintf(stderr, "usage: %s [-n <nn>] [-i x | o] [-t <nn>] [-s <seed>]\n\t-n <filename>\tfrom file\n\t-i <player>\tinteractive\n\t-t <filename>\ttest against a specific opponent\n\t-s <seed>\tspecify random seed\n", argv[0]);
	exit(0);
}

static inline void print_board(const struct reversi *restrict game)
{
	printf("---\n%.*s\n%.*s\n%.*s\n", 3, game->state.board[0], 3, game->state.board[1], 3, game->state.board[2]);
	sleep(1);
}

static int thread_duplex(pthread_t *restrict thread, void *(*start)(void *), int io[static restrict 4])
{
	int pipe_out[2], pipe_in[2];

	if (pipe(pipe_out) < 0)
		return -1;
	if (pipe(pipe_in) < 0)
	{
		close(pipe_out[0]);
		close(pipe_out[1]);
		return -1;
	}

	io[0] = pipe_in[0];
	io[1] = pipe_out[1];
	io[2] = pipe_out[0];
	io[3] = pipe_in[1];

	if (pthread_create(thread, 0, start, io + 2))
	{
		close(io[0]);
		close(io[1]);
		close(io[2]);
		close(io[3]);
		return -1;
	}

	return 0;
}

static inline void network_copy(struct nn *restrict target, const struct nn *restrict source)
{
	memcpy(target->weights, source->weights, source->weights_total * sizeof(*source->weights));
	memcpy(target->biases, source->biases, source->biases_total * sizeof(*source->biases));
}

int main(int argc, char *argv[])
{
	struct player players[1 + AGENTS_COUNT] = {0};
	char ids[1 + AGENTS_COUNT][256]; // TODO don't hardcode this
	int interactive;

	size_t i;

	unsigned games_count = 0;

	double learn_rate = FACTOR;
	double explore_rate = EXPLORE_RATE;

	unsigned opponent_oldest = 0;
	struct optimal *optimal;
	char buffer[256];

	int io_play[4], io_train[TRAIN_THREADS][4], io_test[4];
	pthread_t thread_play, thread_train[TRAIN_THREADS], thread_test;

	//assert(BATCH >= BOARD_SIZE * BOARD_SIZE + 1);

	feenableexcept(FE_INVALID | FE_OVERFLOW);
	srand(time(0));

	interactive = startup(argc, argv, players);

	if (interactive)
	{
		// Play.
		double s = test_game(players, 0, 1, 0);
		printf("winner: %c (score=%.0f)\n", (s > 0) ? 'x' : ((s < 0) ? 'o' : '='), fabs(s));

		if (players[0].utility == &ai_utility)
			free(players[0].network);
		if (players[1].utility == &ai_utility)
			free(players[1].network);

		return 0;
	}

	// Non-interactive game. Train a neural network.
	// Train the network only for one player (training is identical for both).

	printf("gen %u %.3f %.3f | play %u history %u | train %u %u\n", GENERATION, EXPLORE_RATE, FACTOR, PLAY_EPISODES, HISTORY_LIMIT, TRAIN_EPISODES * TRAIN_THREADS, TRAIN_BATCH);

	for(i = 1; i <= AGENTS_COUNT; i += 1)
	{
		players[i].play = &bot_play;
		players[i].utility = &ai_utility;
		players[i].lookahead = 0;

		snprintf(ids[i], sizeof(ids[i]), "<opponent %u>", (unsigned)(i - 1));
		players[i].id = ids[i];

		players[i].network = nn_clone(players[0].network);
		if (!players[i].network)
			abort();
	}

	if (thread_duplex(&thread_play, &main_play, io_play) < 0)
		abort();
	for(i = 0; i < TRAIN_THREADS; i += 1)
		if (thread_duplex(&thread_train[i], &main_train, io_train[i]) < 0)
			abort();
	if (thread_duplex(&thread_test, &main_test, io_test) < 0)
		abort();

	do
	{
		struct train info; // used for play and train threads
		struct gradient gradients[TRAIN_THREADS];
		struct experience *experience;

		info.behavior = players[0];
		info.opponent = players[1 + rand() % AGENTS_COUNT];
		info.explore_rate = explore_rate;
		info.learn_rate = learn_rate;

		// Play against a randomly selected opponent.
		if (write(io_play[1], &info, sizeof(info)) != sizeof(info))
			abort();
		if (read(io_play[0], &experience, sizeof(experience)) != sizeof(experience))
			abort();
		// main_play is now waiting for more data

		// Train from multiple threads in parallel using experience replay.
		info.experience = experience;
		for(i = 0; i < TRAIN_THREADS; i += 1)
			if (write(io_train[i][1], &info, sizeof(info)) != sizeof(info))
				abort();
		for(i = 0; i < TRAIN_THREADS; i += 1)
			if (read(io_train[i][0], &gradients[i], sizeof(gradients[i])) != sizeof(gradients[i]))
				abort();
		// main_train is now waiting for more data

		// Merge the gradient changes from the train threads and update the behavior action-value function.
		// TODO reuse the memory for the weights and biases?
		networks_merge(players[0].network, gradients, TRAIN_THREADS, learn_rate);
		for(i = 0; i < TRAIN_THREADS; i += 1)
		{
			free(gradients[i].weights);
			free(gradients[i].biases);
		}

		// Update and test the target action-value function.
		if (write(io_test[1], &players[0].network, sizeof(players[0].network)) != sizeof(players[0].network))
			abort();
		if (read(io_test[0], &optimal, sizeof(optimal)) != sizeof(optimal)) // blocks until the test thread is ready
			abort();
		// main_test is now waiting for more data

		// Use the best network for future training.
		//network_copy(players[0].network, optimal->best);

		if (!(games_count % GENERATION))
		{
			// Update the action-value function of the oldest opponent.
			//network_copy(players[1 + opponent_oldest].network, optimal->top.data[rand() % optimal->top.count].network);
			network_copy(players[1 + opponent_oldest].network, optimal->best);
			opponent_oldest = (opponent_oldest + 1) % AGENTS_COUNT;
		}

		games_count += TRAIN_EPISODES * TRAIN_THREADS;
		explore_rate = 1 - (double)games_count / GAMES_COUNT;
		learn_rate *= LEARN_UPDATE;

		if (games_count % 4096 == 0)
		{
			printf("# %u / %u episodes; explore=%.3f learn=%.3f\n", games_count, (unsigned)GAMES_COUNT, explore_rate, learn_rate);
			fflush(stdout);
		}
		/*if (games_count % 65536 == 0)
		{
			snprintf(buffer, sizeof(buffer), "# %s vs %s | %u %u | %f %f\n", players[0].id, players[1].id, GENERATION, GAMES_COUNT, EXPLORE_RATE, FACTOR);
			nn_dump_file(optimal->best, "net", buffer);
		}*/
	} while (games_count < GAMES_COUNT);

	// Stop play thread.
	close(io_play[0]);
	close(io_play[1]);
	pthread_join(thread_play, 0);

	// Stop training thread.
	for(i = 0; i < TRAIN_THREADS; i += 1)
	{
		close(io_train[i][0]);
		close(io_train[i][1]);
		pthread_join(thread_train[i], 0);
	}

	snprintf(buffer, sizeof(buffer), "# %s vs %s | %u %u | %f %f\n", players[0].id, players[1].id, GENERATION, GAMES_COUNT, EXPLORE_RATE, FACTOR);
	nn_dump_file(optimal->best, "net", buffer);

	// The buffer for optimal is stored in the test thread and becomes invalid once the thread terminates.
	// Free the associated memory while the pointer is still valid.
	for(i = 0; i < optimal->top.count; i += 1)
		free(optimal->top.data[i].network);

	// Stop test thread.
	close(io_test[0]);
	close(io_test[1]);
	pthread_join(thread_test, 0);

	if (players[0].utility == &ai_utility)
		free(players[0].network);
	if (players[1].utility == &ai_utility)
		free(players[1].network);

	for(i = 2; i <= AGENTS_COUNT; i += 1)
		free(players[i].network);

	return 0;
}
