#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "game.h"

enum {GOAL = 4};

struct scores
{
	int value[2];
};

// WARNING: The engine requires that the board be square so the size is 7x7 and row 0 is kept empty.

struct reversi *game_alloc(void)
{
	struct reversi *new = malloc(sizeof(*new));
	if (!new)
		abort();

	assert(BOARD_SIZE == 7);

	memset(new->state.board, ' ', sizeof(new->state.board));
	new->state.player = 0;
	new->state.time = 0;

	neighbors(new);

	return new;
}

struct reversi *move_alloc(const struct reversi *restrict game, struct position action)
{
	struct reversi *new = memdup(game, sizeof(*new));
	if (!new)
		abort();

	new->state.board[action.y][action.x] = player2sign(game->state.player);
	new->state.time += 1;

	return new;
}

static struct scores lines(const struct state *restrict state, int goal)
{
	int x, y, d;
	unsigned char last;
	unsigned count;
	struct scores total = {0};

	// Check for lines horizontally.
	for(y = BOARD_SIZE - 1; y > 0; y -= 1)
	{
		unsigned filled = 0;

		last = ' ';
		count = 0;

		for(x = 0; x < BOARD_SIZE; x += 1)
		{
			if (state->board[y][x] == last)
			{
				count += 1;
				if ((count == goal) && (last != ' '))
				{
					total.value[last == 'o'] += 1;
					count = 0;
				}
			}
			else
			{
				last = state->board[y][x];
				count = 1;
			}

			filled += (state->board[y][x] != ' ');
		}

		if (!filled)
			break;
	}

	// Check for lines vertically.
	for(x = 0; x < BOARD_SIZE; x += 1)
	{
		last = ' ';
		count = 0;

		for(y = BOARD_SIZE - 1; y > 0; y -= 1)
		{
			if (state->board[y][x] == ' ')
				break;

			if (state->board[y][x] == last)
			{
				count += 1;
				if ((count == goal) && (last != ' '))
				{
					total.value[last == 'o'] += 1;
					count = 0;
				}
			}
			else
			{
				last = state->board[y][x];
				count = 1;
			}
		}
	}

	// Check for lines on diagonals.
	for(d = -(BOARD_SIZE - goal - 1); d < BOARD_SIZE - goal + 1; d += 1)
	{
		last = ' ';
		count = 0;

		// TODO y should actually start at 1 to speed the detection up
		for(x = max(d, 0), y = x - d; (x < BOARD_SIZE) && (y < BOARD_SIZE); x += 1, y += 1)
		{
			if (state->board[y][x] == last)
			{
				count += 1;
				if ((count == goal) && (last != ' '))
				{
					total.value[last == 'o'] += 1;
					count = 0;
				}
			}
			else
			{
				last = state->board[y][x];
				count = 1;
			}
		}
	}
	for(d = -(BOARD_SIZE - goal - 1); d < BOARD_SIZE - goal + 1; d += 1)
	{
		last = ' ';
		count = 0;

		// TODO d can actually start 1 later because y == 0 is not playable
		for(x = max(d, 0), y = BOARD_SIZE - 1 - (x - d); (x < BOARD_SIZE) && (y > 0); x += 1, y -= 1)
		{
			if (state->board[y][x] == last)
			{
				count += 1;
				if ((count == goal) && (last != ' '))
				{
					total.value[last == 'o'] += 1;
					count = 0;
				}
			}
			else
			{
				last = state->board[y][x];
				count = 1;
			}
		}
	}

	return total;
}

void neighbors(struct reversi *restrict game)
{
	int y, x;

	memset(game->moves_board, 0, sizeof(game->moves_board));
	game->moves_count = 0;

	if (game_score(&game->state))
		return;

	for(x = 0; x < BOARD_SIZE; x += 1)
	{
		for(y = BOARD_SIZE - 1; y > 0; y -= 1)
		{
			if (game->state.board[y][x] == ' ')
			{
				game->moves_board[y][x] = 1;
				game->moves[game->moves_count++] = (struct position){y, x};
				break;
			}
		}
	}
}

// TODO distinguish finished from non-finished game in this function
int game_score(const struct state *restrict state)
{
	struct scores scores = lines(state, GOAL);
	return scores.value[0] - scores.value[1];
}

void heuristic_utility0(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;

	// Check if the current player can win in 1 move.
	for(i = 0; i < game->moves_count; i += 1)
	{
		double s;

		struct position move = game->moves[i];
		((struct reversi *)game)->state.board[move.y][move.x] = player2sign(game->state.player);
		s = game_score(&game->state);
		((struct reversi *)game)->state.board[move.y][move.x] = ' ';
		scores[i] = (s != 0); // end game means win for the current player

		if (game->state.player)
			scores[i] = -scores[i];
	}
}

void heuristic_utility1(const struct reversi *restrict game, double *restrict scores, const void *argument)
{
	size_t i;

	// Check if the current player can win in 1 move.
	for(i = 0; i < game->moves_count; i += 1)
	{
		double s;
		struct scores lines3;

		struct position move = game->moves[i];
		((struct reversi *)game)->state.board[move.y][move.x] = player2sign(game->state.player);
		s = game_score(&game->state);
		lines3 = lines(&game->state, GOAL - 1);
		((struct reversi *)game)->state.board[move.y][move.x] = ' ';

		if (s == 0)
		{
			scores[i] = lines3.value[game->state.player] - 4 * lines3.value[!game->state.player];
			if (game->state.player)
				scores[i] = -scores[i];
		}
		else
			scores[i] = game->state.player ? -32 : 32; // end game means win for the current player
	}
}
