# reinforcement-learning

Toy project implementing reinforcement learning from scratch.

Designed for 2-player zero-sum games which can be modelled as MDPs (full observability).
Q-learning (one-step off-policy TD(0) learning with semi-gradient descent).

Supports the following games:
* tic tac toe
* four in a row
* reversi

