struct nn;

struct nn *nn_load_file(const char *restrict filename);
int nn_dump_file(struct nn *restrict network, const char *restrict prefix, const char *restrict comment);
