struct vector;

// WARNING: For LAYER_CONVOLUTION, assume no padding and stride 1.

struct layer
{
	enum {LAYER_INPUT, LAYER_DENSE, LAYER_CONVOLUTION} type;
	enum {ACTIVATION_IDENTITY, ACTIVATION_LOGISTIC, ACTIVATION_SOFTPLUS, ACTIVATION_RELU, ACTIVATION_SOFTMAX} activation;
	size_t x, y, z; // neuron count in each dimension (only the first is used for non-convolutional layers)

	// TODO merge depth and width

	unsigned depth, width, height; // dimensions of the kernel
};

struct nn
{
	size_t layers_count;
	size_t neurons_total; // total count of neurons
	size_t weights_total, biases_total;

	double *weights; // weights for each neuron in each layer
	double *biases; // bias for each neuron in each layer

	struct
	{
		size_t count; // count of neurons in the layer
		size_t offset_neurons, offset_weights, offset_biases;
		double (*activation)(double);
		double (*derivative)(double *restrict, unsigned);

		unsigned kernel_size, kernels_count;
		size_t columns, rows;
		unsigned depth, width, height; // dimensions of the kernel
	} layers[];
};

struct nn_options
{
	double weight_decay; // regularization factor for weight decay
	double learn_rate;
};

struct gradient
{
	double *weights;
	double *biases;
};

void softmax_normalize(double *restrict activations, size_t units_count);

struct nn *nn_init(const struct layer *restrict layers, size_t layers_count);
struct nn *nn_clone(struct nn *restrict source);

struct nn *nn_load(void *data);
void nn_dump(const char *restrict filename, struct nn *restrict network, const char *restrict comment);

const double *nn_regression(const struct nn *restrict network, double *restrict activations);
double loss(const struct nn *restrict network, size_t samples_count, struct vector *restrict y, double *restrict *restrict activations);
double loss_regularization(const struct nn *restrict network, double decay_factor);

struct gradient nn_backpropagate(const struct nn *restrict network, size_t samples_count, struct vector *restrict y, double *restrict *restrict activations, double *restrict gradient, struct gradient *restrict g);
void weights_update(const struct nn *restrict network, const struct nn_options *restrict options, struct gradient g);

// Allocate activation of each neuron in each layer.
static inline double *activations_alloc(const struct nn *restrict network)
{
	return malloc(network->neurons_total * sizeof(double));
}
