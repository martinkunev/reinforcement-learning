CC=gcc
#CFLAGS=-std=c11 -pedantic -g -O0 -D_DEFAULT_SOURCE -Wstrict-aliasing -Wchar-subscripts -Wimplicit -Wsequence-point -Wwrite-strings -Wunused-variable -Werror=vla -Wreturn-type
CFLAGS=-std=c11 -pthread -pedantic -O2 -fstrict-aliasing -fomit-frame-pointer -DNDEBUG -D_DEFAULT_SOURCE -Wstrict-aliasing -Wchar-subscripts -Wimplicit -Wsequence-point -Wwrite-strings -Wunused-variable -Werror=vla -Wreturn-type -Werror=discarded-qualifiers -Werror=incompatible-pointer-types -Werror=int-conversion -Werror=format -Wsign-conversion
#CFLAGS=-I /usr/local/include/python3.9
#LDFLAGS=-lpython3.9
LDFLAGS=-pthread
LDLIBS=-lm -latomic

all: tictactoe reversi four

square: nn.o io.o vector.o utils.o game.o square.o

reversi: nn.o io.o vector.o utils.o game.o train.o benchmark.o ai.o reversi.o

tictactoe: nn.o io.o vector.o utils.o game.o train.o benchmark.o ai.o tictactoe.o

four: nn.o io.o vector.o utils.o game.o train.o benchmark.o ai.o four.o

test: nn.o utils.o test.o vector.o

strip:
	strip -R .comment -R .note tictactoe
	strip -R .comment -R .note reversi

python:
	./build.py build
	ln -f -s build/lib.linux-x86_64-3.9/reversibot.cpython-39-x86_64-linux-gnu.so

clean:
	rm -rf build
	rm -f *.o
	rm -f reversi tictactoe
	rm -f reversibot.cpython-39-x86_64-linux-gnu.so
