#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "nn.h"
#include "game.h"
#include "train.h"
#include "vector.h"

//#define DECAY_FACTOR (double)1e-7
#define DECAY_FACTOR (double)1e-9

//#define PERFORMANCE 1

// Remember samples in a sliding window.
static void experience_add(struct experience *restrict history, const struct state *restrict current, struct position action, const struct reversi *restrict next)
{
	struct state *state;
	int game_over = (next->moves_count == 0);

	history->last = (history->last + 1) % (sizeof(history->samples) / sizeof(*history->samples));

	state = &history->samples[history->last].state;
	memcpy(state->board, current->board, sizeof(current->board));
	state->player = current->player;
	state->time = current->time;

	state = &history->samples[history->last].state_next;
	memcpy(state->board, next->state.board, sizeof(next->state.board));
	state->player = next->state.player;
	state->time = next->state.time;

	//history->samples[history->last].action = action_index(action);
	history->samples[history->last].action = action;
	history->samples[history->last].game_over = game_over;
	history->samples[history->last].reward = game_over ? game_score(&next->state) : 0;

	if (history->count < (sizeof(history->samples) / sizeof(*history->samples)))
		history->count += 1;
}

static void experience_generate(const struct train *restrict train, struct experience *restrict history)
{
	// hack - store two copies of the pointer to the first player
	// this way agents behaves like an array of the two players even when player_first == 1

	size_t e;
	const struct player *const agents[3] = {&train->behavior, &train->opponent, &train->behavior};

	assert(PLAY_EPISODES % 2 == 0);

	for(e = 0; e < PLAY_EPISODES; e += 1)
	{
		int player_first = e % 2;
		struct reversi *game, *new;

		// Play.
		game = game_alloc();
		do
		{
			const struct player *agent = agents[game->state.player + player_first];
			struct score score = (*agent->play)(game, agents + player_first, train->explore_rate);

			new = transition_alloc(game, score.action);

/*if (new->state.time == 1)
	printf("%s vs %s | %u,%u | ", agents[(game->state.player + player_first) % 2]->id, agents[1 - (game->state.player + player_first) % 2]->id, score.action.y, score.action.x);*/

			experience_add(history, &game->state, score.action, new);

			free(game);
			game = new;
		} while (game->moves_count);
		free(game);
	}
}

void networks_merge(struct nn *restrict target, struct gradient *restrict gradients, size_t count, double learn_rate)
{
	struct nn_options options = {.weight_decay = DECAY_FACTOR, .learn_rate = learn_rate};

	size_t i, j;

	for(i = 0; i < target->weights_total; i += 1)
		for(j = 1; j < count; j += 1)
			gradients[0].weights[i] += gradients[j].weights[i];
	for(i = 0; i < target->biases_total; i += 1)
		for(j = 1; j < count; j += 1)
			gradients[0].biases[i] += gradients[j].biases[i];

	weights_update(target, &options, gradients[0]);
}

// Produces a network trained on the history.
static struct gradient train_generation(const struct train *restrict train, struct experience *restrict history, double *restrict *restrict activations, struct vector *restrict y)
{
	size_t i, e;
	struct gradient g, *gp = 0;
	struct nn *target = nn_clone(train->behavior.network); // TODO if TRAIN_EPISODES == 1 I don't need to change the network; no need to clone

	if (!target)
		return (struct gradient){0};

	if (history->count < TRAIN_BATCH)
	{
		free(target);
		return (struct gradient){0};
	}

	for(e = 0; e < TRAIN_EPISODES; e += 1)
	{
		// Train using experience replay to improve convergence and avoid overfitting.

		unsigned samples_count;

		//struct nn_options options = {.weight_decay = DECAY_FACTOR, .learn_rate = train->learn_rate};

		// Select a batch.
		for(samples_count = 0; samples_count < TRAIN_BATCH; samples_count += 1)
		{
			double *q, s;
			const double *output;
			double *sample_y = vector_get(y, samples_count, 0);
			size_t augment = (size_t)rand() % 8; // randomness for data augmentation
			struct position action;

			i = (size_t)rand() % history->count;

			// Initialize sample.
			input_set(activations[samples_count], &history->samples[i].state, augment);
			output = nn_regression(target, activations[samples_count]);
			memcpy(sample_y, output, OUTPUT_SIZE * sizeof(*output));
			action = position_transform(history->samples[i].action, augment);

			// Update target policy Q value for the chosen action.
			// WARNING: Assume reward is 0 for all non-terminal states.
			q = &sample_y[action_index(action)];
			if (history->samples[i].game_over)
				s = history->samples[i].reward;
			else
				s = ai_score(target, &history->samples[i].state_next);
			if (history->samples[i].state.player)
				s = -s; // score must be with respect to the current player
			*q = DISCOUNT_FACTOR * s;
		}

		// Update the target policy.
		g = nn_backpropagate(target, samples_count, y, activations, activations[TRAIN_BATCH], gp);
		gp = &g;

		// Soft-update the target policy toward the behavior policy.
		/*for(i = 0; i < target->biases_total; i += 1)
			target->biases[i] = (nn->biases[i] - target->biases[i]) * UPDATE_RATE;
		for(i = 0; i < target->weights_total; i += 1)
			target->weights[i] = (nn->weights[i] - target->weights[i]) * UPDATE_RATE;*/
	}

	free(target);
	return g;
}

void *main_play(void *argument)
{
	int *io = argument;
	struct train train;
	static struct experience history;
	struct experience *experience = &history;

	assert(TRAIN_BATCH <= HISTORY_LIMIT);
	assert(sizeof(train) <= PIPE_BUF);

	history.last = (size_t)-1;
	history.count = 0;

	while (1)
	{
		if (read(io[0], &train, sizeof(train)) != sizeof(train))
			break;

#if defined(PERFORMANCE)
		struct timespec tic, toc;
		clock_gettime(CLOCK_MONOTONIC, &tic);
#endif
		experience_generate(&train, &history);
#if defined(PERFORMANCE)
		clock_gettime(CLOCK_MONOTONIC, &toc);
		printf("play time (ms): %.4f\n", (double)((toc.tv_sec - tic.tv_sec) * (unsigned long long)1000000000 + toc.tv_nsec - tic.tv_nsec) / 1000000);
#endif

		if (write(io[1], &experience, sizeof(experience)) != sizeof(experience))
			break;
	}

	close(io[0]);
	close(io[1]);

	return 0;
}

void *main_train(void *argument)
{
	int *io = argument;
	struct train train;

	double *activations[TRAIN_BATCH + 1] = {0}; // 1 entry in the end for the gradients
	struct vector *y = 0;
	size_t i;

	assert(TRAIN_BATCH <= HISTORY_LIMIT);
	assert(sizeof(train) <= PIPE_BUF);

	y = vector_alloc(TRAIN_BATCH, OUTPUT_SIZE);
	if (!y)
		return 0;

	while (1)
	{
		struct gradient gradient;

		if (read(io[0], &train, sizeof(train)) != sizeof(train))
			break;
		if (!activations[0])
			for(i = 0; i <= TRAIN_BATCH; i += 1)
			{
				activations[i] = activations_alloc(train.behavior.network);
				if (!activations[i])
					goto finally;
			}

#if defined(PERFORMANCE)
		struct timespec tic, toc;
		clock_gettime(CLOCK_MONOTONIC, &tic);
#endif
		gradient = train_generation(&train, train.experience, activations, y);
		if (!gradient.weights)
			break;
#if defined(PERFORMANCE)
		clock_gettime(CLOCK_MONOTONIC, &toc);
		printf("train time (ms): %.4f\n", (double)((toc.tv_sec - tic.tv_sec) * (unsigned long long)1000000000 + toc.tv_nsec - tic.tv_nsec) / 1000000);
#endif

		if (write(io[1], &gradient, sizeof(gradient)) != sizeof(gradient))
			break;
	}

finally:
	for(i = 0; i <= TRAIN_BATCH; i += 1)
		free(activations[i]);
	free(y);

	close(io[0]);
	close(io[1]);

	return 0;
}
