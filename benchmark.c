#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

#include "game.h"
#include "utils.h"
#include "nn.h"

#define HEAP_SOURCE
#include "benchmark.h"

enum {TESTS = 4096, TESTS_HISTORY = 128, TEST_THREADS = 8};

enum {HISTORY_PERIOD = 32, PROGRESS_PERIOD = 32};

enum {ELO_START = 2000, ELO_SPREAD = 800, ELO_SPEED = 64};

struct progress_info
{
	struct
	{
		struct nn *network;
		unsigned generation;
	} history[HISTORY_CAPACITY];
	size_t history_count;
	size_t history_index;
	struct nn *network;
	unsigned generation;
	unsigned period;
};

static inline double duration(struct timespec tic)
{
	struct timespec toc;
	clock_gettime(CLOCK_MONOTONIC, &toc);
	return ((toc.tv_sec - tic.tv_sec) * (long long)1000000000 + toc.tv_nsec - tic.tv_nsec) / 1000000.0;
}

// Returns the score from a single game.
// Positive and negative score mean that player 0 and player 1 won respectively.
int (test_game)(const struct player *restrict players, int player_first, double explore_rate_start, double explore_rate, int verbose)
{
	const struct player *const agents[] = {&players[player_first], &players[!player_first]};
	struct reversi *game = game_alloc();
	int score;

	if (verbose)
		printf("========== %c\n", player2sign(verbose - 1));
	do
	{
		const struct player *current = agents[game->state.player];
		struct score score = (*current->play)(game, agents, game->state.time ? explore_rate : explore_rate_start);
		struct reversi *new = transition_alloc(game, score.action);
		free(game);
		game = new;
		if (verbose)
			print(game);
	} while (game->moves_count);

	score = game_score(&game->state);
	free(game);
	return (player_first == 0 ? score : -score);
}

static inline void score_update(double score, int *restrict wins, int *restrict draws, int *restrict losses)
{
	switch (sign(score))
	{
	case 1:
		*wins += 1;
		return;

	case 0:
		*draws += 1;
		return;

	case -1:
		*losses += 1;
		return;

	default:
		assert(0);
	}
}

/*static inline double agent_score(int wins, int draws, int losses)
{
	// Make players risk averse by discouraging moves which could win against a weak player and lose against a strong player.
	return wins - losses * 2;
}*/

static inline void elo_update(double score, struct agent_model *p0, struct agent_model *p1, int p0_h1, int p1_h1)
{
	double expected_p0 = 1 / (1 + pow(10.0, (p1->elo - p0->elo) / ELO_SPREAD));
	double expected_p1 = 1 / (1 + pow(10.0, (p0->elo - p1->elo) / ELO_SPREAD));

	double score01 = (sign(score) + 1) / 2.0; // scale score to the interval [0, 1]

	//printf("%.1f (%.1f)\n", fabs(ELO_SPEED * (score01 - expected_p0)), score01);

	p0->elo += ELO_SPEED * (score01 - expected_p0);
	p1->elo += ELO_SPEED * ((1 - score01) - expected_p1);

	if (p0_h1 != p1_h1)
	{
		if (p0_h1)
		{
			p1->vs_h1 += (score01 == 0);
			p1->vs_h1 += 0.5 * (score01 == 0.5);
			p1->episodes += 1;
		}
		else if (p1_h1)
		{
			p0->vs_h1 += (score01 == 1);
			p0->vs_h1 += 0.5 * (score01 == 0.5);
			p0->episodes += 1;
		}
	}
}

static void test_networks(struct heap *restrict top, struct agent_model (*agents)[3])
{
	struct player players[2] = {{.play = &bot_play}, {.play = &bot_play}};
	size_t i;

	// TODO maybe I should reset elo on each test
	for(i = 0; i < top->count; i += 1)
		top->data[i].elo = ELO_START;
	(*agents)[0].elo = ELO_START;
	(*agents)[1].elo = ELO_START;
	(*agents)[2].elo = ELO_START;

	// TODO do I need the heuristics? they can only guide up to a certain level of play

	/*if (top->count < 2)
		return;*/

	//players[0].utility = &ai_utility;
	//players[1].utility = &ai_utility;

	// Select pairs of policies and compare each pair in 2 games (alternate who plays first).

	for(i = 0; i < TESTS; i += 2 * TEST_THREADS)
	{
		//size_t a = (size_t)rand() % (top->count + 3), b = (size_t)rand() % (top->count + 3);
		size_t a = (size_t)rand() % top->count, b = (size_t)rand() % top->count;
		struct agent_model *agent_a, *agent_b;

		/*switch (a)
		{
		case 0:
			agent_a = &(*agents)[a];
			players[0].utility = &zero_utility;
			players[0].id = "zero";
			break;

		case 1:
			agent_a = &(*agents)[a];
			players[0].utility = &heuristic_utility0;
			players[0].id = "heu0";
			break;

		case 2:
			agent_a = &(*agents)[a];
			players[0].utility = &heuristic_utility1;
			players[0].id = "heu1";
			break;

		default:
			agent_a = &top->data[a - 3];
			players[0].utility = &ai_utility;
			players[0].id = "nn";
			break;
		}*/
			agent_a = &top->data[a];
			players[0].utility = &ai_utility;
			players[0].id = "nn";
		players[0].network = agent_a->network;

		/*switch (b)
		{
		case 0:
			agent_b = &(*agents)[b];
			players[1].utility = &zero_utility;
			players[1].id = "zero";
			break;

		case 1:
			agent_b = &(*agents)[b];
			players[1].utility = &heuristic_utility0;
			players[1].id = "heu0";
			break;

		case 2:
			agent_b = &(*agents)[b];
			players[1].utility = &heuristic_utility1;
			players[1].id = "heu1";
			break;

		default:
			agent_b = &top->data[b - 3];
			players[1].utility = &ai_utility;
			players[1].id = "nn";
			break;
		}*/
			agent_b = &top->data[b];
			players[1].utility = &ai_utility;
			players[1].id = "nn";
		players[1].network = agent_b->network;

		elo_update(test_game(players, 0, 1, 0, 0), agent_a, agent_b, a == 2, b == 2);
		elo_update(test_game(players, 1, 1, 0, 0), agent_a, agent_b, a == 2, b == 2);
	}

	// TODO should I add lookahead
}

void print_progress(struct nn *restrict network)
{
	static int best_draws = 0, best_losses = 2 * TESTS_HISTORY;

	struct player players[2];
	int wins, draws, losses;
	size_t i;

	players[0].play = &bot_play;
	players[0].utility = &ai_utility;
	players[0].lookahead = 0;
	players[0].network = network;

	players[1].play = &bot_play;

	players[1].lookahead = 0;
	players[1].utility = &zero_utility;
	players[1].id = "<random (lookahead 0)>";
	wins = draws = losses = 0;
	for(i = 0; i < TESTS_HISTORY; i += 1)
	{
		score_update(test_game(players, 0, 1, 0), &wins, &draws, &losses);
		score_update(test_game(players, 1, 1, 0), &wins, &draws, &losses);
	}
	printf("> 0 %.2f %.2f %.2f %s\n", wins / 2.0 / TESTS_HISTORY, draws / 2.0 / TESTS_HISTORY, losses / 2.0 / TESTS_HISTORY, players[1].id);

	players[1].lookahead = 0;
	players[1].utility = &heuristic_utility0;
	players[1].id = "<heuristic 0 (lookahead 0)>";
	wins = draws = losses = 0;
	for(i = 0; i < TESTS_HISTORY; i += 1)
	{
		score_update(test_game(players, 0, 1, 0), &wins, &draws, &losses);
		score_update(test_game(players, 1, 1, 0), &wins, &draws, &losses);
	}
	printf("> 1 %.2f %.2f %.2f %s\n", wins / 2.0 / TESTS_HISTORY, draws / 2.0 / TESTS_HISTORY, losses / 2.0 / TESTS_HISTORY, players[1].id);

	players[1].lookahead = 0;
	players[1].id = "<heuristic 1 (lookahead 0)>";
	players[1].utility = &heuristic_utility1;
	wins = draws = losses = 0;
	for(i = 0; i < TESTS_HISTORY; i += 1)
	{
		//score_update(test_game(players, 0, 1, 0, 2), &wins, &draws, &losses);
		//score_update(test_game(players, 1, 1, 0, 1), &wins, &draws, &losses);
		score_update(test_game(players, 0, 1, 0), &wins, &draws, &losses);
		score_update(test_game(players, 1, 1, 0), &wins, &draws, &losses);
	}
	printf("> 2 %.2f %.2f %.2f %s\n", wins / 2.0 / TESTS_HISTORY, draws / 2.0 / TESTS_HISTORY, losses / 2.0 / TESTS_HISTORY, players[1].id);

	if ((losses < best_losses) || (losses == best_losses && draws < best_draws))
	{
		char buffer[256];
		snprintf(buffer, sizeof(buffer), "# > 2 %.2f %.2f %.2f %s\n", wins / 2.0 / TESTS_HISTORY, draws / 2.0 / TESTS_HISTORY, losses / 2.0 / TESTS_HISTORY, players[1].id);
		nn_dump_file(network, "net", buffer);

		best_losses = losses;
		best_draws = draws;
	}
}

static void *main_benchmark(void *argument)
{
	struct optimal *optimal = argument;
	test_networks(&optimal->top, &optimal->agents_fixed);
	return 0;
}

static void *main_progress(void *argument)
{
	struct progress_info *progress = argument;
	struct player players[2] = {{.play = &bot_play, .utility = &ai_utility}, {.play = &bot_play, .utility = &ai_utility}};
	size_t i, j;
	unsigned history_generation = 0, period = HISTORY_PERIOD;

	players[0].network = progress->network;
	for(i = 0; i < progress->history_count; i += 1)
	{
		size_t index = (HISTORY_CAPACITY + progress->history_index - progress->history_count + i) % HISTORY_CAPACITY;
		int wins = 0, draws = 0, losses = 0;

		players[1].network = progress->history[index].network;
		for(j = 0; j < TESTS_HISTORY; j += 1)
		{
			score_update(test_game(players, 0, 1, 0), &wins, &draws, &losses);
			score_update(test_game(players, 1, 1, 0), &wins, &draws, &losses);
		}

		//printf("~ %u vs %u | +%u %u -%u\n", progress->generation, (unsigned)(progress->generation - (progress->generation % HISTORY_PERIOD) - (progress->history_count - i - 1) * HISTORY_PERIOD), wins, draws, losses);
		printf("~ %u vs %u | +%u %u -%u\n", progress->generation, history_generation, wins, draws, losses);
		history_generation += period;
		period *= 2;
	}

	// Test the best network against hard-coded policies to provide a reference point for performance.
	print_progress(progress->network);

	fflush(stdout);

	if (!(progress->generation % progress->period))
	{
		progress->history[progress->history_index].network = progress->network;
		progress->history[progress->history_index].generation = progress->generation;

		progress->history_index = (progress->history_index + 1) % HISTORY_CAPACITY;
		if (progress->history_count < HISTORY_CAPACITY)
			progress->history_count += 1;

		progress->period *= 2;
	}

	return 0;
}

void *main_test(void *argument)
{
	int *io = argument;
	struct optimal optimal, *optimal_address = &optimal;

	struct nn *networks[1];
	size_t i;

	unsigned generation;
	static struct progress_info progress;
	int progress_running = 0;
	pthread_t thread_progress;

	assert(HISTORY_PERIOD >= PROGRESS_PERIOD);

	progress.history_count = 0;
	progress.history_index = 0;
	progress.period = HISTORY_PERIOD;

	optimal.top = (struct heap){.data = optimal.agents};
	optimal.agents_fixed[0] = (struct agent_model){.elo = ELO_START};
	optimal.agents_fixed[1] = (struct agent_model){.elo = ELO_START};
	optimal.agents_fixed[2] = (struct agent_model){.elo = ELO_START};
	optimal.best = 0;

	for(generation = 0; 1; generation += 1)
	{
		struct agent_model *best;
		double rating_best = 0, rating_optimal;
		double elo_optimal = 0;
		pthread_t threads[TEST_THREADS];

		if (read(io[0], &networks, sizeof(networks)) != sizeof(networks))
			break;

#if defined(PERFORMANCE)
		struct timespec tic;
		clock_gettime(CLOCK_MONOTONIC, &tic);
#endif

		for(i = 0; i < 1; i += 1)
		{
			struct nn *copy = nn_clone(networks[i]);
			if (!copy)
				goto finally;
			optimal.top.data[optimal.top.count++] = (struct agent_model){.network = copy, .elo = ELO_START, .episodes = 1};
		}

		if (progress_running)
		{
			pthread_join(thread_progress, 0);
			progress_running = 0;
		}

		assert(optimal.top.count + 1 + progress.history_count <= sizeof(optimal.agents) / sizeof(*optimal.agents));

		// TODO this takes random networks from history and adds them to the heap
		if (progress.history_count)
		{
			//for(i = 0; i < AGENTS_HISTORY; i += 1)
			for(i = 0; i < progress.history_count; i += 1)
			{
				size_t index = i; // (size_t)rand() % progress.history_count;
				struct nn *old = nn_clone(progress.history[index].network);
				if (!old)
					goto finally;
				assert(optimal.top.count + 1 <= sizeof(optimal.agents) / sizeof(*optimal.agents));
				optimal.top.data[optimal.top.count++] = (struct agent_model){.network = old, .elo = ELO_START, .episodes = 1};
			}
		}

#if defined(PERFORMANCE)
		printf("test before (ms): %.4f\n", duration(tic));
		fflush(stdout);
		clock_gettime(CLOCK_MONOTONIC, &tic);
#endif

		// Measure agent performance.
		for(i = 0; i < sizeof(threads) / sizeof(*threads); i += 1)
			pthread_create(&threads[i], 0, main_benchmark, &optimal);
		for(i = 0; i < sizeof(threads) / sizeof(*threads); i += 1)
			pthread_join(threads[i], 0);

		/*printf("> 0 %.0f <random (lookahead 0)>\n> 1 %.0f <heuristic 0 (lookahead 0)>\n> 2 %.0f <heuristic 1 (lookahead 0)>\n", optimal.agents_fixed[0].elo, optimal.agents_fixed[1].elo, optimal.agents_fixed[2].elo);
		putchar('~');
		for(i = 0; i < optimal.top.count; i += 1)
		{
			printf(" %.0f", optimal.top.data[i].elo);
			if ((generation % HISTORY_PERIOD == 0) && optimal.top.data[i].vs_h1)
				printf("(%.1f)", (double)optimal.top.data[i].vs_h1 / optimal.top.data[i].episodes);
		}
		putchar('\n');*/

#if defined(PERFORMANCE)
		printf("test time (ms): %.4f\n", duration(tic));
		fflush(stdout);
		clock_gettime(CLOCK_MONOTONIC, &tic);
#endif

		// Keep the top performing agents in a heap.
		heap_heapify(&optimal.top);
		while (optimal.top.count > AGENTS_COUNT)
		{
			// Remove the worst performing network to free space in the buffer.
			free(optimal.top.data[0].network);
			heap_pop(&optimal.top);
		}

		best = &optimal.top.data[0];
		//rating_best = (double)best->vs_h1 / best->episodes;
		for(i = 1; i < optimal.top.count; i += 1) // TODO I need to loop only the leafs (except if I need the elo of the best)
		{
			//double rating = (double)optimal.top.data[i].vs_h1 / optimal.top.data[i].episodes;
			if (optimal.top.data[i].elo > best->elo)
			//if (rating > rating_best)
			{
				best = &optimal.top.data[i];
				//rating_best = rating;
			}
			if (optimal.top.data[i].network == optimal.best)
				elo_optimal = optimal.top.data[i].elo;
				//rating_optimal = rating;
		}

		// Use a coefficient 0.95 to filter out noise when updating the best network.
		if (best->elo * 0.95 >= elo_optimal)
		//if (rating_optimal * 0.9 >= rating_best)
			optimal.best = best->network;
		//printf(": %.0f\n", best->elo);

		if (generation % PROGRESS_PERIOD == 0)
		{
			// Copy the optimal network before communicating with the main thread.
			// The main thread could free the memory for optimal networks.
			progress.network = nn_clone(optimal.best);
			if (!progress.network)
				break;
		}

#if defined(PERFORMANCE)
		printf("test after (ms): %.4f\n", duration(tic));
		fflush(stdout);
#endif

		if (write(io[1], &optimal_address, sizeof(optimal_address)) != sizeof(optimal_address))
			break;

		// Store history of past best networks.
		if (generation % PROGRESS_PERIOD == 0)
		{
			progress.generation = generation;
			pthread_create(&thread_progress, 0, main_progress, &progress);
			progress_running = 1;
		}
	}

finally:
	for(i = 0; i < progress.history_count; i += 1)
		free(progress.history[i].network);

	close(io[0]);
	close(io[1]);

	return 0;
}
